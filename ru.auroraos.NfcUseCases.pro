# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.NfcUseCases

CONFIG += \
    auroraapp \
    auroraapp_i18n \

TRANSLATIONS += \
    translations/ru.auroraos.NfcUseCases.ts \
    translations/ru.auroraos.NfcUseCases-ru.ts \

SOURCES += \
    src/main.cpp \

DISTFILES += \
    rpm/ru.auroraos.NfcUseCases.spec \
    LICENSE.BSD-3-Clause.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    AUTHORS.md \
    README.md \
    README.ru.md \

include (submodules/pcschandler/pcschandler.pri)
include (submodules/nfcdhandler/nfcdhandler.pri)

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172
