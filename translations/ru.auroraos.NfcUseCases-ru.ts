<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;В проекте приведен пример работы со стеком NFC.&lt;/p&gt;
                            &lt;p&gt;Основная цель - показать не только, какие функции доступны для
                            работы с этими API, но и как их правильно использовать.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="45"/>
        <source>The 3-Clause BSD License</source>
        <translation>The 3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2021-2023 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="28"/>
        <source>PCSC</source>
        <translation>PCSC</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="35"/>
        <source>NFCD</source>
        <translation>NFCD</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="29"/>
        <source>Working with NFC cards using the PCSC Lite API</source>
        <translation>Работа с NFC картами с помощью PCSC Lite API</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Working with NFC cards using NFCD DBus interfaces</source>
        <translation>Работа с NFC картами с помощью NFCD DBus интерфейсов</translation>
    </message>
</context>
<context>
    <name>NfcUseCases</name>
    <message>
        <location filename="../qml/NfcUseCases.qml" line="10"/>
        <source>NFC Use Cases</source>
        <translation>Работа с NFC</translation>
    </message>
</context>
<context>
    <name>NfcdAdapterPage</name>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="48"/>
        <source>Adapter info</source>
        <translation>Информация об адаптере</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="53"/>
        <source>Interface version:</source>
        <translation>Версия интерфейса:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="59"/>
        <source>Enabled:</source>
        <translation>Включенный:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="65"/>
        <source>Powered:</source>
        <translation>Питаемый:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="71"/>
        <source>Target present:</source>
        <translation>Цель представлена:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="77"/>
        <source>Supported modes:</source>
        <translation>Поддерживаемые режимы:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="83"/>
        <source>Mode:</source>
        <translation>Режим:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="89"/>
        <source>Available tags</source>
        <translation>Доступные метки</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="111"/>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="146"/>
        <source>Tag &apos;%1&apos;</source>
        <translation>Метка &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="153"/>
        <source>Protocol:</source>
        <translation>Протокол:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="160"/>
        <source>Technology:</source>
        <translation>Технология:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="167"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
</context>
<context>
    <name>NfcdDaemonPage</name>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="66"/>
        <source>Available adapters</source>
        <translation>Доступные адаптеры</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="94"/>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="129"/>
        <source>Adapter &apos;%1&apos;</source>
        <translation>Адаптер &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="136"/>
        <source>Enabled:</source>
        <translation>Включенный:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="143"/>
        <source>Powered:</source>
        <translation>Питаемый:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="49"/>
        <source>Daemon info</source>
        <translation>Информация о демоне</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="54"/>
        <source>Daemon version:</source>
        <translation>Версия демона:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="60"/>
        <source>Interface version:</source>
        <translation>Версия интерфейса:</translation>
    </message>
</context>
<context>
    <name>NfcdIsoDepIface</name>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="51"/>
        <source>IsoDep info</source>
        <translation>Информация о IsoDep</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="56"/>
        <source>Interface version:</source>
        <translation>Версия интерфейса:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="62"/>
        <source>Request</source>
        <translation>Запрос</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="69"/>
        <source>CLA</source>
        <translation>CLA</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="77"/>
        <source>INS</source>
        <translation>INS</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="85"/>
        <source>P1</source>
        <translation>P1</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="93"/>
        <source>P2</source>
        <translation>P2</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="101"/>
        <source>Data</source>
        <translation>Данные</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="109"/>
        <source>Length</source>
        <translation>Длина</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="115"/>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="122"/>
        <source>SW1:</source>
        <translation>SW1:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="129"/>
        <source>SW2:</source>
        <translation>SW2:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="136"/>
        <source>Response:</source>
        <translation>Ответ:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="167"/>
        <source>Transmit</source>
        <translation>Передать</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="192"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
</context>
<context>
    <name>NfcdNdefPage</name>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="52"/>
        <source>NDEF record info</source>
        <translation>Информация о NDEF записи</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="57"/>
        <source>Interface version:</source>
        <translation>Версия интерфейса:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="63"/>
        <source>Flags:</source>
        <translation>Флаги:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="70"/>
        <source>Type name format:</source>
        <translation>Формат имени типа:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="76"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="82"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="88"/>
        <source>Payload:</source>
        <translation>Полезная нагрузка:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="94"/>
        <source>Raw data:</source>
        <translation>Сырые данные:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="100"/>
        <source>Interfaces:</source>
        <translation>Интерфейсы:</translation>
    </message>
</context>
<context>
    <name>NfcdTagIface</name>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="52"/>
        <source>Tag info</source>
        <translation>Информация о метке</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="57"/>
        <source>Interface version:</source>
        <translation>Версия интерфейса:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="63"/>
        <source>Present:</source>
        <translation>Представленна:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="69"/>
        <source>Protocol:</source>
        <translation>Протокол:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="75"/>
        <source>Technology:</source>
        <translation>Технология:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="81"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="87"/>
        <source>Poll parameters:</source>
        <translation>Параметры опроса:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="94"/>
        <source>NDEF records</source>
        <translation>NDEF записи</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="138"/>
        <source>Data</source>
        <translation>Данные</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="183"/>
        <source>Acquire</source>
        <translation>Захватить</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="191"/>
        <source>Release</source>
        <translation>Отпустить</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="145"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="146"/>
        <source>Response:</source>
        <translation>Ответ:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="122"/>
        <source>Record &apos;%1&apos;</source>
        <translation>Запись &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="131"/>
        <source>Function &apos;transceive&apos;</source>
        <translation>Функция &apos;transceive&apos;</translation>
    </message>
</context>
<context>
    <name>NfcdTagPage</name>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="49"/>
        <source>Interface:</source>
        <translation>Интерфейс:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="50"/>
        <source>Select the desired interface</source>
        <translation>Выберите нужный интерфейс</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="60"/>
        <source>org.sailfishos.nfc.Tag</source>
        <translation>org.sailfishos.nfc.Tag</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="66"/>
        <source>org.sailfishos.nfc.TagType2</source>
        <translation>org.sailfishos.nfc.TagType2</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="72"/>
        <source>org.sailfishos.nfc.IsoDep</source>
        <translation>org.sailfishos.nfc.IsoDep</translation>
    </message>
</context>
<context>
    <name>NfcdTagType2Iface</name>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="52"/>
        <source>Interface version:</source>
        <translation>Версия интерфейса:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="58"/>
        <source>Block size:</source>
        <translation>Размер блока:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="64"/>
        <source>Data size:</source>
        <translation>Размер данных:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="70"/>
        <source>Serial:</source>
        <translation>Серийный номер:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="76"/>
        <source>Function &apos;write&apos;</source>
        <translation>Функция &apos;write&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="83"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="161"/>
        <source>Sector</source>
        <translation>Сектор</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="91"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="169"/>
        <source>Block</source>
        <translation>Блок</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="99"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="135"/>
        <source>Data</source>
        <translation>Данные</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="109"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="144"/>
        <source>Write</source>
        <translation>Записать</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="110"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="145"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="178"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="210"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="224"/>
        <source>Result:</source>
        <translation>Результат:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="120"/>
        <source>Function &apos;writeData&apos;</source>
        <translation>Функция &apos;writeData&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="127"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="193"/>
        <source>Offset</source>
        <translation>Отступ</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="154"/>
        <source>Function &apos;read&apos;</source>
        <translation>Функция &apos;read&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="177"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="209"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="223"/>
        <source>Read</source>
        <translation>Прочитать</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="186"/>
        <source>Function &apos;readData&apos;</source>
        <translation>Функция &apos;readData&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="201"/>
        <source>Max bytes</source>
        <translation>Максимум байт</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="218"/>
        <source>Function &apos;readAllData&apos;</source>
        <translation>Функция &apos;readAllData&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="48"/>
        <source>Tag type 2 info</source>
        <translation>Информация о метке 2-го типа</translation>
    </message>
</context>
<context>
    <name>PcscPage</name>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="33"/>
        <source>Attach the tag</source>
        <translation>Приложите тег</translation>
    </message>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="62"/>
        <source>Attribute:</source>
        <translation>Атрибут:</translation>
    </message>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="69"/>
        <source>State:</source>
        <translation>Состояние:</translation>
    </message>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="76"/>
        <source>Checksum:</source>
        <translation>Контрольная сумма:</translation>
    </message>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="83"/>
        <source>Protocol:</source>
        <translation>Протокол:</translation>
    </message>
</context>
<context>
    <name>UiStrings</name>
    <message>
        <location filename="../qml/js/UiStrings.js" line="5"/>
        <source>yes</source>
        <translation>да</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="5"/>
        <source>no</source>
        <translation>нет</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="28"/>
        <source>P2PInitiator</source>
        <translation>P2PInitiator</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="30"/>
        <source>ReaderWriter</source>
        <translation>ReaderWriter</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="32"/>
        <source>P2PTarget</source>
        <translation>P2PTarget</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="34"/>
        <source>CardEmulation</source>
        <translation>CardEmulation</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="46"/>
        <location filename="../qml/js/UiStrings.js" line="66"/>
        <source>unknown</source>
        <translation>неизвестно</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="48"/>
        <source>type 1</source>
        <translation>type 1</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="50"/>
        <source>type 2</source>
        <translation>type 2</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="52"/>
        <source>type 3</source>
        <translation>type 3</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="54"/>
        <source>type 4A</source>
        <translation>type 4A</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="56"/>
        <source>type 4B</source>
        <translation>type 4B</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="58"/>
        <source>DEP</source>
        <translation>DEP</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="68"/>
        <source>NFC A</source>
        <translation>NFC A</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="70"/>
        <source>NFC B</source>
        <translation>NFC B</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="72"/>
        <source>NFC F</source>
        <translation>NFC F</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="84"/>
        <source>first record</source>
        <translation>первая запись</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="86"/>
        <source>last record</source>
        <translation>последняя запись</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="98"/>
        <source>TNF0</source>
        <translation>TNF0</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="100"/>
        <source>TNF1</source>
        <translation>TNF1</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="102"/>
        <source>TNF2</source>
        <translation>TNF2</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="104"/>
        <source>TNF3</source>
        <translation>TNF3</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="106"/>
        <source>TNF4</source>
        <translation>TNF4</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="36"/>
        <location filename="../qml/js/UiStrings.js" line="60"/>
        <location filename="../qml/js/UiStrings.js" line="74"/>
        <location filename="../qml/js/UiStrings.js" line="88"/>
        <location filename="../qml/js/UiStrings.js" line="108"/>
        <source>empty</source>
        <translation>пусто</translation>
    </message>
</context>
</TS>
