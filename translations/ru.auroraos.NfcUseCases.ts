<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;The project provides an example of working with NFC stack.&lt;/p&gt;
                            &lt;p&gt;The main purpose is to show not only what features are available to
                            work with these API, but also how to use them correctly.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="45"/>
        <source>The 3-Clause BSD License</source>
        <translation>The 3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2021-2023 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="28"/>
        <source>PCSC</source>
        <translation>PCSC</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="35"/>
        <source>NFCD</source>
        <translation>NFCD</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="29"/>
        <source>Working with NFC cards using the PCSC Lite API</source>
        <translation>Working with NFC cards using the PCSC Lite API</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Working with NFC cards using NFCD DBus interfaces</source>
        <translation>Working with NFC cards using NFCD DBus interfaces</translation>
    </message>
</context>
<context>
    <name>NfcUseCases</name>
    <message>
        <location filename="../qml/NfcUseCases.qml" line="10"/>
        <source>NFC Use Cases</source>
        <translation>NFC Use Cases</translation>
    </message>
</context>
<context>
    <name>NfcdAdapterPage</name>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="48"/>
        <source>Adapter info</source>
        <translation>Adapter info</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="53"/>
        <source>Interface version:</source>
        <translation>Interface version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="59"/>
        <source>Enabled:</source>
        <translation>Enabled:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="65"/>
        <source>Powered:</source>
        <translation>Powered:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="71"/>
        <source>Target present:</source>
        <translation>Target present:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="77"/>
        <source>Supported modes:</source>
        <translation>Supported modes:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="83"/>
        <source>Mode:</source>
        <translation>Mode:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="89"/>
        <source>Available tags</source>
        <translation>Available tags</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="111"/>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="146"/>
        <source>Tag &apos;%1&apos;</source>
        <translation>Tag &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="153"/>
        <source>Protocol:</source>
        <translation>Protocol:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="160"/>
        <source>Technology:</source>
        <translation>Technology:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdAdapterPage.qml" line="167"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
</context>
<context>
    <name>NfcdDaemonPage</name>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="66"/>
        <source>Available adapters</source>
        <translation>Available adapters</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="94"/>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="129"/>
        <source>Adapter &apos;%1&apos;</source>
        <translation>Adapter &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="136"/>
        <source>Enabled:</source>
        <translation>Enabled:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="143"/>
        <source>Powered:</source>
        <translation>Powered:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="49"/>
        <source>Daemon info</source>
        <translation>Daemon info</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="54"/>
        <source>Daemon version:</source>
        <translation>Daemon version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdDaemonPage.qml" line="60"/>
        <source>Interface version:</source>
        <translation>Interface version:</translation>
    </message>
</context>
<context>
    <name>NfcdIsoDepIface</name>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="51"/>
        <source>IsoDep info</source>
        <translation>IsoDep info</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="56"/>
        <source>Interface version:</source>
        <translation>Interface version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="62"/>
        <source>Request</source>
        <translation>Request</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="69"/>
        <source>CLA</source>
        <translation>CLA</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="77"/>
        <source>INS</source>
        <translation>INS</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="85"/>
        <source>P1</source>
        <translation>P1</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="93"/>
        <source>P2</source>
        <translation>P2</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="101"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="109"/>
        <source>Length</source>
        <translation>Length</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="115"/>
        <source>Result</source>
        <translation>Result</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="122"/>
        <source>SW1:</source>
        <translation>SW1:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="129"/>
        <source>SW2:</source>
        <translation>SW2:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="136"/>
        <source>Response:</source>
        <translation>Response:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="167"/>
        <source>Transmit</source>
        <translation>Transmit</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdIsoDepIface.qml" line="192"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
</context>
<context>
    <name>NfcdNdefPage</name>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="52"/>
        <source>NDEF record info</source>
        <translation>NDEF record info</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="57"/>
        <source>Interface version:</source>
        <translation>Interface version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="63"/>
        <source>Flags:</source>
        <translation>Flags:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="70"/>
        <source>Type name format:</source>
        <translation>Type name format:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="76"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="82"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="88"/>
        <source>Payload:</source>
        <translation>Payload:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="94"/>
        <source>Raw data:</source>
        <translation>Raw data:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdNdefPage.qml" line="100"/>
        <source>Interfaces:</source>
        <translation>Interfaces:</translation>
    </message>
</context>
<context>
    <name>NfcdTagIface</name>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="52"/>
        <source>Tag info</source>
        <translation>Tag info</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="57"/>
        <source>Interface version:</source>
        <translation>Interface version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="63"/>
        <source>Present:</source>
        <translation>Present:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="69"/>
        <source>Protocol:</source>
        <translation>Protocol:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="75"/>
        <source>Technology:</source>
        <translation>Technology:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="81"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="87"/>
        <source>Poll parameters:</source>
        <translation>Poll parameters:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="94"/>
        <source>NDEF records</source>
        <translation>NDEF records</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="138"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="183"/>
        <source>Acquire</source>
        <translation>Acquire</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="191"/>
        <source>Release</source>
        <translation>Release</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="145"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="146"/>
        <source>Response:</source>
        <translation>Response:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="122"/>
        <source>Record &apos;%1&apos;</source>
        <translation>Record &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagIface.qml" line="131"/>
        <source>Function &apos;transceive&apos;</source>
        <translation>Function &apos;transceive&apos;</translation>
    </message>
</context>
<context>
    <name>NfcdTagPage</name>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="49"/>
        <source>Interface:</source>
        <translation>Interface:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="50"/>
        <source>Select the desired interface</source>
        <translation>Select the desired interface</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="60"/>
        <source>org.sailfishos.nfc.Tag</source>
        <translation>org.sailfishos.nfc.Tag</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="66"/>
        <source>org.sailfishos.nfc.TagType2</source>
        <translation>org.sailfishos.nfc.TagType2</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagPage.qml" line="72"/>
        <source>org.sailfishos.nfc.IsoDep</source>
        <translation>org.sailfishos.nfc.IsoDep</translation>
    </message>
</context>
<context>
    <name>NfcdTagType2Iface</name>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="52"/>
        <source>Interface version:</source>
        <translation>Interface version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="58"/>
        <source>Block size:</source>
        <translation>Block size:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="64"/>
        <source>Data size:</source>
        <translation>Data size:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="70"/>
        <source>Serial:</source>
        <translation>Serial:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="76"/>
        <source>Function &apos;write&apos;</source>
        <translation>Function &apos;write&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="83"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="161"/>
        <source>Sector</source>
        <translation>Sector</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="91"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="169"/>
        <source>Block</source>
        <translation>Block</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="99"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="135"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="109"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="144"/>
        <source>Write</source>
        <translation>Write</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="110"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="145"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="178"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="210"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="224"/>
        <source>Result:</source>
        <translation>Result:</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="120"/>
        <source>Function &apos;writeData&apos;</source>
        <translation>Function &apos;writeData&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="127"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="193"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="154"/>
        <source>Function &apos;read&apos;</source>
        <translation>Function &apos;read&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="177"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="209"/>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="223"/>
        <source>Read</source>
        <translation>Read</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="186"/>
        <source>Function &apos;readData&apos;</source>
        <translation>Function &apos;readData&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="201"/>
        <source>Max bytes</source>
        <translation>Max bytes</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="218"/>
        <source>Function &apos;readAllData&apos;</source>
        <translation>Function &apos;readAllData&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/NfcdTagType2Iface.qml" line="48"/>
        <source>Tag type 2 info</source>
        <translation>Tag type 2 info</translation>
    </message>
</context>
<context>
    <name>PcscPage</name>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="33"/>
        <source>Attach the tag</source>
        <translation>Attach the tag</translation>
    </message>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="62"/>
        <source>Attribute:</source>
        <translation>Attribute:</translation>
    </message>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="69"/>
        <source>State:</source>
        <translation>State:</translation>
    </message>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="76"/>
        <source>Checksum:</source>
        <translation>Checksum:</translation>
    </message>
    <message>
        <location filename="../qml/pages/PcscPage.qml" line="83"/>
        <source>Protocol:</source>
        <translation>Protocol:</translation>
    </message>
</context>
<context>
    <name>UiStrings</name>
    <message>
        <location filename="../qml/js/UiStrings.js" line="5"/>
        <source>yes</source>
        <translation>yes</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="5"/>
        <source>no</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="28"/>
        <source>P2PInitiator</source>
        <translation>P2PInitiator</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="30"/>
        <source>ReaderWriter</source>
        <translation>ReaderWriter</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="32"/>
        <source>P2PTarget</source>
        <translation>P2PTarget</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="34"/>
        <source>CardEmulation</source>
        <translation>CardEmulation</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="46"/>
        <location filename="../qml/js/UiStrings.js" line="66"/>
        <source>unknown</source>
        <translation>unknown</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="48"/>
        <source>type 1</source>
        <translation>type 1</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="50"/>
        <source>type 2</source>
        <translation>type 2</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="52"/>
        <source>type 3</source>
        <translation>type 3</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="54"/>
        <source>type 4A</source>
        <translation>type 4A</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="56"/>
        <source>type 4B</source>
        <translation>type 4B</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="58"/>
        <source>DEP</source>
        <translation>DEP</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="68"/>
        <source>NFC A</source>
        <translation>NFC A</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="70"/>
        <source>NFC B</source>
        <translation>NFC B</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="72"/>
        <source>NFC F</source>
        <translation>NFC F</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="84"/>
        <source>first record</source>
        <translation>first record</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="86"/>
        <source>last record</source>
        <translation>last record</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="98"/>
        <source>TNF0</source>
        <translation>TNF0</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="100"/>
        <source>TNF1</source>
        <translation>TNF1</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="102"/>
        <source>TNF2</source>
        <translation>TNF2</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="104"/>
        <source>TNF3</source>
        <translation>TNF3</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="106"/>
        <source>TNF4</source>
        <translation>TNF4</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="36"/>
        <location filename="../qml/js/UiStrings.js" line="60"/>
        <location filename="../qml/js/UiStrings.js" line="74"/>
        <location filename="../qml/js/UiStrings.js" line="88"/>
        <location filename="../qml/js/UiStrings.js" line="108"/>
        <source>empty</source>
        <translation>empty</translation>
    </message>
</context>
</TS>
