// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

function yesOrNo(flag) {
    return flag ? qsTr("yes") : qsTr("no");
}

function mapToString(map) {
    var stringList = [];
    for (var item in map) {
        stringList.push(" · " + item + ": " + map[item]);
    }

    return stringList.join("\n");
}

function listToString(list) {
    var stringList = [];
    for (var i = 0; i < list.length; ++i) {
        stringList.push(" · " + list[i]);
    }

    return stringList.join("\n");
}

function mode(mode) {
    if (mode == NfcdMode.P2PInitiator) {
        return qsTr("P2PInitiator");
    } else if (mode == NfcdMode.ReaderWriter) {
        return qsTr("ReaderWriter");
    } else if (mode == NfcdMode.P2PTarget) {
        return qsTr("P2PTarget");
    } else if (mode == NfcdMode.CardEmulation) {
        return qsTr("CardEmulation");
    } else {
        return qsTr("empty");
    }
}

function modes(modes) {
    return listToString(modes.map(UiStrings.mode));
}

function tagProtocol(tagProtocol) {
    if (tagProtocol == NfcdTagProtocol.UnknownProtocol) {
        return qsTr("unknown");
    } else if (tagProtocol == NfcdTagProtocol.Type1) {
        return qsTr("type 1");
    } else if (tagProtocol == NfcdTagProtocol.Type2) {
        return qsTr("type 2");
    } else if (tagProtocol == NfcdTagProtocol.Type3) {
        return qsTr("type 3");
    } else if (tagProtocol == NfcdTagProtocol.Type4A) {
        return qsTr("type 4A");
    } else if (tagProtocol == NfcdTagProtocol.Type4B) {
        return qsTr("type 4B");
    } else if (tagProtocol == NfcdTagProtocol.DEP) {
        return qsTr("DEP");
    } else {
        return qsTr("empty");
    }
}

function tagTechnology(tagTechnology) {
    if (tagTechnology == NfcdTagTechnology.UnknownTechnology) {
        return qsTr("unknown");
    } else if (tagTechnology == NfcdTagTechnology.NfcA) {
        return qsTr("NFC A");
    } else if (tagTechnology == NfcdTagTechnology.NfcB) {
        return qsTr("NFC B");
    } else if (tagTechnology == NfcdTagTechnology.NfcF) {
        return qsTr("NFC F");
    } else {
        return qsTr("empty");
    }
}

function tagType(tagType) {
    return tagType;
}

function ndefFlag(ndefFlag) {
    if (ndefFlag == NfcdNdefFlag.FirstRecord) {
        return qsTr("first record");
    } else if (ndefFlag == NfcdNdefFlag.LastRecord) {
        return qsTr("last record");
    } else {
        return qsTr("empty");
    }
}

function ndefFlags(ndefFlags) {
    return listToString(ndefFlags.map(UiStrings.ndefFlag));
}

function ndefTNF(ndefTNF) {
    if (ndefTNF == NfcdNdefTNF.TNF0) {
        return qsTr("TNF0");
    } else if (ndefTNF == NfcdNdefTNF.TNF1) {
        return qsTr("TNF1");
    } else if (ndefTNF == NfcdNdefTNF.TNF2) {
        return qsTr("TNF2");
    } else if (ndefTNF == NfcdNdefTNF.TNF3) {
        return qsTr("TNF3");
    } else if (ndefTNF == NfcdNdefTNF.TNF4) {
        return qsTr("TNF4");
    } else {
        return qsTr("empty");
    }
}
