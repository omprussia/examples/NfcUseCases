// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.NfcUseCases 1.0
import "../components"

Item {
    id: nfcdIsoDepIface

    property string path

    objectName: "nfcdIsoDepIface"

    NfcdIsoDepHandler {
        id: nfcdIsoDepHandler

        objectName: "nfcdIsoDepHandler"
        tagPath: nfcdIsoDepIface.path
    }

    SilicaFlickable {
        objectName: "flickableArea"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: buttonsArea.top
            bottomMargin: Theme.paddingMedium
        }
        contentHeight: mainColumn.implicitHeight
        clip: true

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: mainColumn

            objectName: "mainColumn"
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingMedium

            SectionHeader {
                objectName: "isoDepInfoSection"
                text: qsTr("IsoDep info")
            }

            AlignedLabelItem {
                objectName: "interfaceVersionItem"
                label: qsTr("Interface version:")
                value: nfcdIsoDepHandler.interfaceVersion
            }

            SectionHeader {
                objectName: "requestSection"
                text: qsTr("Request")
            }

            HexValueItem {
                id: claValueItem

                objectName: "claValueItem"
                label: qsTr("CLA")
                text: "0x00"
            }

            HexValueItem {
                id: insValueItem

                objectName: "insValueItem"
                label: qsTr("INS")
                text: "0xA4"
            }

            HexValueItem {
                id: p1ValueItem

                objectName: "p1ValueItem"
                label: qsTr("P1")
                text: "0x04"
            }

            HexValueItem {
                id: p2ValueItem

                objectName: "p2ValueItem"
                label: qsTr("P2")
                text: "0x00"
            }

            HexValueItem {
                id: dataValueItem

                objectName: "dataValueItem"
                label: qsTr("Data")
                text: "0x325041592E5359532E444446303100"
            }

            HexValueItem {
                id: leValueItem

                objectName: "leValueItem"
                label: qsTr("Length")
                text: "0x0E"
            }

            SectionHeader {
                objectName: "resultSection"
                text: qsTr("Result")
            }

            AlignedLabelItem {
                id: sw1ResultItem

                objectName: "sw1ResultItem"
                label: qsTr("SW1:")
            }

            AlignedLabelItem {
                id: sw2ResultItem

                objectName: "sw2ResultItem"
                label: qsTr("SW2:")
            }

            AlignedLabelItem {
                id: responseResultItem

                objectName: "responseResultItem"
                label: qsTr("Response:")
            }
        }
    }

    Rectangle {
        id: buttonsArea

        objectName: "buttonsArea"
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            topMargin: Theme.paddingMedium
        }
        height: buttonsLayout.implicitHeight + Theme.paddingMedium * 2
        color: Theme.highlightDimmerColor

        ButtonLayout {
            id: buttonsLayout

            objectName: "buttonsLayout"
            anchors {
                fill: parent
                topMargin: Theme.paddingMedium
            }
            rowSpacing: Theme.paddingMedium
            columnSpacing: Theme.paddingMedium

            Button {
                objectName: "transmitButton"
                text: qsTr("Transmit")

                onClicked: {
                    /*
                     * Example: SELECT PPSE = '00 A4 04 00 0E 32 50 41 59 2E 53 59 53 2E 44 44 46 30 31 00'
                     * 0x00 - CLA Class
                     * 0xA4 - INS Instruction
                     * 0x04 - P1  Parameter 1
                     * 0x00 - P2  Parameter 2
                     * 0x32, 0x50, 0x41, 0x59, 0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31, 0x00 (ending marker) - AID
                     * 0x0E - Length
                     */
                    var result = nfcdIsoDepHandler.transmit(Number(claValueItem.text), Number(insValueItem.text),
                                                            Number(p1ValueItem.text), Number(p2ValueItem.text),
                                                            dataValueItem.text, Number(leValueItem.text));

                    sw1ResultItem.value = "0x" + result.sw1.toString(16).toUpperCase();
                    sw2ResultItem.value = "0x" + result.sw2.toString(16).toUpperCase();
                    if (result.response.length > 0)
                        responseResultItem.value = "0x" + result.response.toString(16).toUpperCase();
                }
            }

            Button {
                objectName: "resetButton"
                text: qsTr("Reset")

                onClicked: nfcdIsoDepHandler.reset()
            }
        }
    }
}
