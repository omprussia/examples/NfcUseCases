// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.NfcUseCases 1.0
import "../js/UiStrings.js" as UiStrings
import "../components"

Page {
    id: nfcdDaemonPage

    property string title
    property string path
    property string description

    objectName: "nfcdDaemonPage"

    NfcdDaemonHandler {
        id: nfcdDaemonHandler

        objectName: "nfcdDaemonHandler"
    }

    SilicaFlickable {
        objectName: "flickableArea"
        anchors.fill: parent
        contentHeight: mainColumn.implicitHeight
        clip: true

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: mainColumn

            objectName: "mainColumn"
            anchors.fill: parent
            spacing: Theme.paddingMedium

            PageHeader {
                objectName: "pageHeader"
                title: nfcdDaemonPage.title
                description: nfcdDaemonPage.description
            }

            SectionHeader {
                objectName: "daemonInfoSection"
                text: qsTr("Daemon info")
            }

            AlignedLabelItem {
                objectName: "daemonVersionItem"
                label: qsTr("Daemon version:")
                value: nfcdDaemonHandler.daemonVersion
            }

            AlignedLabelItem {
                objectName: "interfaceVersionItem"
                label: qsTr("Interface version:")
                value: nfcdDaemonHandler.interfaceVersion
            }

            SectionHeader {
                objectName: "availableAdaptersSection"
                text: qsTr("Available adapters")
            }

            SilicaListView {
                objectName: "adaptersView"
                width: parent.width
                height: contentHeight
                delegate: adapterItemDelegateComponent
                model: nfcdDaemonHandler.adaptersModel
                clip: true
            }

            Component {
                id: adapterItemDelegateComponent

                ListItem {
                    id: adapterItem

                    objectName: "adapterItem_%1".arg(model.index)
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    contentHeight: adapterItemColumn.height + Theme.paddingLarge
                    enabled: model.enabled && model.powered
                    opacity: enabled ? 1.0 : Theme.opacityLow

                    onClicked: pageStack.push(Qt.resolvedUrl("NfcdAdapterPage.qml"), {
                            "title": qsTr("Adapter '%1'").arg(model.path),
                            "path": model.path
                        })

                    HighlightImage {
                        id: adapterItemIcon

                        objectName: "adapterItemIcon"
                        anchors {
                            left: parent.left
                            leftMargin: Theme.horizontalPageMargin
                        }
                        source: "image://theme/icon-m-nfc"
                        sourceSize {
                            width: Theme.iconSizeLarge
                            height: Theme.iconSizeLarge
                        }
                        highlightColor: palette.highlightColor
                        highlighted: adapterItem.highlighted
                    }

                    Column {
                        id: adapterItemColumn

                        objectName: "adapterItemColumn"
                        anchors {
                            left: adapterItemIcon.right
                            right: parent.right
                            leftMargin: Theme.horizontalPageMargin
                            rightMargin: Theme.horizontalPageMargin
                            verticalCenter: parent.verticalCenter
                        }

                        Label {
                            objectName: "nameItem"
                            text: qsTr("Adapter '%1'").arg(model.path)
                            color: adapterItem.highlighted ? palette.highlightColor : palette.primaryColor
                            font.bold: true
                        }

                        AlignedLabelItem {
                            objectName: "enabledItem"
                            label: qsTr("Enabled:")
                            value: UiStrings.yesOrNo(model.enabled)
                            leftMargin: 0
                        }

                        AlignedLabelItem {
                            objectName: "poweredItem"
                            label: qsTr("Powered:")
                            value: UiStrings.yesOrNo(model.powered)
                            leftMargin: 0
                        }
                    }
                }
            }
        }
    }
}
