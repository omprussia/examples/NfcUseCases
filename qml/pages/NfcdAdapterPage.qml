// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.NfcUseCases 1.0
import "../js/UiStrings.js" as UiStrings
import "../components"

Page {
    id: nfcdAdapterPage

    property string title
    property string path

    objectName: "nfcdAdapterPage"

    NfcdAdapterHandler {
        id: nfcdAdapterHandler

        objectName: "nfcdAdapterHandler"
        adapterPath: path
    }

    SilicaFlickable {
        objectName: "flickableArea"
        anchors.fill: parent
        contentHeight: mainColumn.implicitHeight
        clip: true

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: mainColumn

            objectName: "mainColumn"
            anchors.fill: parent
            spacing: Theme.paddingMedium

            PageHeader {
                objectName: "pageHeader"
                title: nfcdAdapterPage.title
            }

            SectionHeader {
                objectName: "infoSection"
                text: qsTr("Adapter info")
            }

            AlignedLabelItem {
                objectName: "interfaceVersionItem"
                label: qsTr("Interface version:")
                value: nfcdAdapterHandler.interfaceVersion
            }

            AlignedLabelItem {
                objectName: "enabledItem"
                label: qsTr("Enabled:")
                value: UiStrings.yesOrNo(nfcdAdapterHandler.enabled)
            }

            AlignedLabelItem {
                objectName: "poweredItem"
                label: qsTr("Powered:")
                value: UiStrings.yesOrNo(nfcdAdapterHandler.powered)
            }

            AlignedLabelItem {
                objectName: "targetPresentItem"
                label: qsTr("Target present:")
                value: UiStrings.yesOrNo(nfcdAdapterHandler.targetPresent)
            }

            AlignedLabelItem {
                objectName: "supportedModesItem"
                label: qsTr("Supported modes:")
                value: UiStrings.modes(nfcdAdapterHandler.supportedModes)
            }

            AlignedLabelItem {
                objectName: "modeItem"
                label: qsTr("Mode:")
                value: UiStrings.mode(nfcdAdapterHandler.mode)
            }

            SectionHeader {
                objectName: "availableTagsSection"
                text: qsTr("Available tags")
            }

            SilicaListView {
                objectName: "tagsView"
                width: parent.width
                height: contentHeight
                delegate: tagItemDelegateComponent
                model: nfcdAdapterHandler.tagsModel
                clip: true
            }

            Component {
                id: tagItemDelegateComponent

                ListItem {
                    id: tagItem

                    objectName: "tagItem_%1".arg(model.index)
                    width: parent.width
                    contentHeight: tagItemColumn.height + Theme.paddingLarge
                    onClicked: pageStack.push(Qt.resolvedUrl("NfcdTagPage.qml"), {
                            "title": qsTr("Tag '%1'").arg(model.path),
                            "path": model.path
                        })

                    HighlightImage {
                        id: tagItemIcon

                        objectName: "tagItemIcon"
                        anchors {
                            left: parent.left
                            leftMargin: Theme.horizontalPageMargin
                        }
                        source: "image://theme/icon-m-device"
                        sourceSize {
                            width: Theme.iconSizeLarge
                            height: Theme.iconSizeLarge
                        }
                        highlightColor: palette.highlightColor
                        highlighted: tagItem.highlighted
                    }

                    Column {
                        id: tagItemColumn

                        objectName: "tagItemColumn"
                        anchors {
                            left: tagItemIcon.right
                            right: parent.right
                            leftMargin: Theme.horizontalPageMargin
                            rightMargin: Theme.horizontalPageMargin
                            verticalCenter: parent.verticalCenter
                        }

                        Label {
                            objectName: "nameItem"
                            text: qsTr("Tag '%1'").arg(model.path)
                            color: tagItem.highlighted ? palette.highlightColor : palette.primaryColor
                            font.bold: true
                        }

                        AlignedLabelItem {
                            objectName: "protocolItem"
                            label: qsTr("Protocol:")
                            value: UiStrings.tagProtocol(model.protocol)
                            leftMargin: 0
                        }

                        AlignedLabelItem {
                            objectName: "technologyItem"
                            label: qsTr("Technology:")
                            value: UiStrings.tagTechnology(model.technology)
                            leftMargin: 0
                        }

                        AlignedLabelItem {
                            objectName: "typeItem"
                            label: qsTr("Type:")
                            value: UiStrings.tagType(model.type)
                            leftMargin: 0
                        }
                    }
                }
            }
        }
    }
}
