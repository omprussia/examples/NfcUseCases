// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.NfcUseCases 1.0
import "../components"

Page {
    id: nfcdTagPage

    property string title
    property string path
    property bool isoDepIsAvailable: false
    property bool rwType2IsAvailable: false

    objectName: "nfcdTagPage"

    NfcdTagHandler {
        id: nfcdTagHandler

        objectName: "nfcdTagHandler"
        tagPath: path

        onRemoved: pageStack.pop(pageStack.previousPage(nfcdTagPage), PageStackAction.Animated)
    }

    Column {
        id: mainColumn

        objectName: "mainColumn"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        spacing: Theme.paddingMedium
        height: implicitHeight

        PageHeader {
            objectName: "pageHeader"
            title: nfcdTagPage.title
        }

        ComboBox {
            id: interfacesCmbBox

            objectName: "interfacesCmbBox"
            width: parent.width
            label: qsTr("Interface:")
            description: qsTr("Select the desired interface")
            menu: ContextMenu {
                Repeater {
                    model: ListModel {
                        id: itemsModel

                        objectName: "itemsModel"

                        ListElement {
                            objectName: "tagIfaceItem"
                            text: qsTr("org.sailfishos.nfc.Tag")
                            page: "NfcdTagIface.qml"
                        }

                        ListElement {
                            objectName: "tagType2IfaceItem"
                            text: qsTr("org.sailfishos.nfc.TagType2")
                            page: "NfcdTagType2Iface.qml"
                        }

                        ListElement {
                            objectName: "isoDepIfaceItem"
                            text: qsTr("org.sailfishos.nfc.IsoDep")
                            page: "NfcdIsoDepIface.qml"
                        }
                    }

                    MenuItem {
                        objectName: "menuItem"
                        text: model.text
                        enabled: interfacesCmbBox.interfaceIsAvailable(model.text)
                    }
                }
            }

            onCurrentIndexChanged: setupIfaceView(currentIndex)
            Component.onCompleted: setupIfaceView(currentIndex)

            function interfaceIsAvailable(name) {
                for (var i = 0; i < nfcdTagHandler.interfaces.length; ++i)
                    if (nfcdTagHandler.interfaces[i] === name)
                        return true;
                return false;
            }

            function setupIfaceView(index) {
                var page = itemsModel.get(index).page;
                contentLoader.setSource(Qt.resolvedUrl(page), {
                        "path": nfcdTagPage.path
                    });
            }
        }
    }

    Loader {
        id: contentLoader

        objectName: "contentLoader"
        anchors {
            top: mainColumn.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }
}
