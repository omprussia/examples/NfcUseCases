// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    objectName: "mainPage"

    SilicaListView {
        objectName: "useCasesList"
        anchors.fill: parent
        header: pageHeaderComponent
        model: listItemsModel
        delegate: listItemDelegateComponent

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }
    }

    ListModel {
        id: listItemsModel

        objectName: "listItemsModel"

        ListElement {
            objectName: "pcscItem"
            name: qsTr("PCSC")
            description: qsTr("Working with NFC cards using the PCSC Lite API")
            page: "PcscPage.qml"
        }

        ListElement {
            objectName: "nfcdItem"
            name: qsTr("NFCD")
            description: qsTr("Working with NFC cards using NFCD DBus interfaces")
            page: "NfcdDaemonPage.qml"
        }
    }

    Component {
        id: pageHeaderComponent

        PageHeader {
            id: pageHeader

            objectName: "pageHeader"
            title: appWindow.appName
            extraContent.children: [
                IconButton {
                    objectName: "pageHeaderButton"
                    anchors.verticalCenter: parent.verticalCenter
                    icon {
                        source: "image://theme/icon-m-about"
                        sourceSize {
                            width: Theme.iconSizeMedium
                            height: Theme.iconSizeMedium
                        }
                    }

                    onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                }
            ]
        }
    }

    Component {
        id: listItemDelegateComponent

        ListItem {
            id: listItemDelegate

            objectName: "listItemDelegate"
            contentHeight: column.height

            onClicked: pageStack.push(Qt.resolvedUrl(model.page), {
                    "title": model.name,
                    "description": model.description
                })

            Column {
                id: column

                objectName: "column"
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.horizontalPageMargin
                }
                topPadding: Theme.paddingMedium
                bottomPadding: Theme.paddingMedium

                Label {
                    objectName: "nameLabel"
                    text: model.name
                    width: parent.width
                }

                Label {
                    objectName: "descriptionLabel"
                    text: model.description
                    width: parent.width
                    color: listItemDelegate.highlighted ? palette.highlightColor : palette.secondaryColor
                    font.pixelSize: Theme.fontSizeSmall
                    truncationMode: TruncationMode.Fade
                }
            }
        }
    }
}
