// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.NfcUseCases 1.0
import "../js/UiStrings.js" as UiStrings
import "../components"

Page {
    id: nfcdNdefPage

    property string title
    property string path

    objectName: "nfcdNdefPage"

    NfcdNdefHandler {
        id: nfcdNdefHandler

        objectName: "nfcdNdefHandler"
        recordPath: path
    }

    SilicaFlickable {
        objectName: "flickableArea"
        anchors.fill: parent
        contentHeight: mainColumn.implicitHeight
        clip: true

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: mainColumn

            objectName: "mainColumn"
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingMedium

            PageHeader {
                objectName: "pageHeader"
                title: nfcdNdefPage.title
            }

            SectionHeader {
                objectName: "recordInfoSection"
                text: qsTr("NDEF record info")
            }

            AlignedLabelItem {
                objectName: "interfaceVersionItem"
                label: qsTr("Interface version:")
                value: nfcdNdefHandler.interfaceVersion
            }

            AlignedLabelItem {
                objectName: "flagsItem"
                label: qsTr("Flags:")
                value: UiStrings.ndefFlags(nfcdNdefHandler.flags)
                forceValueBelow: true
            }

            AlignedLabelItem {
                objectName: "typeNameFormatItem"
                label: qsTr("Type name format:")
                value: UiStrings.ndefTNF(nfcdNdefHandler.typeNameFormat)
            }

            AlignedLabelItem {
                objectName: "typeItem"
                label: qsTr("Type:")
                value: nfcdNdefHandler.type
            }

            AlignedLabelItem {
                objectName: "idItem"
                label: qsTr("ID:")
                value: nfcdNdefHandler.id
            }

            AlignedLabelItem {
                objectName: "payloadItem"
                label: qsTr("Payload:")
                value: nfcdNdefHandler.payload
            }

            AlignedLabelItem {
                objectName: "rawDataItem"
                label: qsTr("Raw data:")
                value: nfcdNdefHandler.rawData
            }

            AlignedLabelItem {
                objectName: "interfacesItem"
                label: qsTr("Interfaces:")
                value: UiStrings.listToString(nfcdNdefHandler.interfaces)
                forceValueBelow: true
            }
        }
    }
}
