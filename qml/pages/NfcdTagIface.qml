// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.NfcUseCases 1.0
import "../js/UiStrings.js" as UiStrings
import "../components"

Item {
    id: nfcdTagIface

    property string path

    objectName: "nfcdTagIface"

    NfcdTagHandler {
        id: nfcdTagHandler

        objectName: "nfcdTagHandler"
        tagPath: nfcdTagIface.path
    }

    SilicaFlickable {
        objectName: "flickableArea"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: buttonsArea.top
            bottomMargin: Theme.paddingMedium
        }
        contentHeight: mainColumn.implicitHeight
        clip: true

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: mainColumn

            objectName: "mainColumn"
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingMedium

            SectionHeader {
                objectName: "tagInfoSection"
                text: qsTr("Tag info")
            }

            AlignedLabelItem {
                objectName: "interfaceVersionItem"
                label: qsTr("Interface version:")
                value: nfcdTagHandler.interfaceVersion
            }

            AlignedLabelItem {
                objectName: "presentItem"
                label: qsTr("Present:")
                value: UiStrings.yesOrNo(nfcdTagHandler.present)
            }

            AlignedLabelItem {
                objectName: "protocolItem"
                label: qsTr("Protocol:")
                value: UiStrings.tagProtocol(nfcdTagHandler.protocol)
            }

            AlignedLabelItem {
                objectName: "technologyItem"
                label: qsTr("Technology:")
                value: UiStrings.tagTechnology(nfcdTagHandler.technology)
            }

            AlignedLabelItem {
                objectName: "typeItem"
                label: qsTr("Type:")
                value: UiStrings.tagType(nfcdTagHandler.type)
            }

            AlignedLabelItem {
                objectName: "pollParametersItem"
                label: qsTr("Poll parameters:")
                value: UiStrings.mapToString(nfcdTagHandler.pollParameters)
                forceValueBelow: true
            }

            SectionHeader {
                objectName: "recordsSection"
                text: qsTr("NDEF records")
            }

            SilicaListView {
                id: listView

                objectName: "recordsView"
                width: parent.width
                height: contentHeight
                model: nfcdTagHandler.ndefRecords
                delegate: Component {
                    ListItem {
                        id: clickableItem

                        objectName: "clickableItem"
                        width: listView.width
                        height: Theme.itemSizeSmall

                        Label {
                            objectName: "itemLabel"
                            anchors.verticalCenter: parent.verticalCenter
                            leftPadding: Theme.horizontalPageMargin
                            rightPadding: Theme.horizontalPageMargin
                            color: clickableItem.highlighted ? palette.highlightColor : palette.primaryColor
                            text: modelData
                        }

                        onClicked: pageStack.push(Qt.resolvedUrl("NfcdNdefPage.qml"), {
                                "title": qsTr("Record '%1'").arg(modelData),
                                "path": modelData
                            })
                    }
                }
            }

            SectionHeader {
                objectName: "funcTransceiveSection"
                text: qsTr("Function 'transceive'")
            }

            HexValueItem {
                id: transceiveDataValueItem

                objectName: "transceiveDataValueItem"
                label: qsTr("Data")
                text: "0x0"
            }

            DetailButtonItem {
                objectName: "transceiveResultItem"
                buttonEnabled: !transceiveDataValueItem.errorHighlight
                buttonText: qsTr("Send")
                resultLabel: qsTr("Response:")

                onClicked: {
                    var result = nfcdTagHandler.transceive(transceiveDataValueItem.text);
                    if (result.length > 0)
                        resultValue = "0x" + result.toString(16).toUpperCase();
                }
            }
        }
    }

    Rectangle {
        id: buttonsArea

        objectName: "buttonsArea"
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            topMargin: Theme.paddingMedium
        }
        height: buttonsLayout.implicitHeight + Theme.paddingMedium * 2
        color: Theme.highlightDimmerColor

        ButtonLayout {
            id: buttonsLayout

            objectName: "buttonsLayout"
            anchors {
                fill: parent
                topMargin: Theme.paddingMedium
            }
            rowSpacing: Theme.paddingMedium
            columnSpacing: Theme.paddingMedium

            Button {
                objectName: "acquireButton"
                text: qsTr("Acquire")
                ButtonLayout.newLine: false

                onClicked: nfcdTagHandler.acquire(true)
            }

            Button {
                objectName: "releaseButton"
                text: qsTr("Release")
                ButtonLayout.newLine: false

                onClicked: nfcdTagHandler.release()
            }
        }
    }
}
