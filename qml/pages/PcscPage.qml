// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.NfcUseCases 1.0

Page {
    id: pcscPage

    property string title
    property string description

    objectName: "pcscPage"

    PcscHandler {
        id: pcscHandler

        objectName: "pcscHandler"
    }

    SilicaFlickable {
        objectName: "flickableArea"
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        ViewPlaceholder {
            objectName: "viewPlaceholder"
            enabled: !pcscHandler.tagDetected
            text: qsTr("Attach the tag")
        }

        Column {
            id: column

            objectName: "column"
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingLarge

            PageHeader {
                objectName: "pageHeader"
                title: pcscPage.title
                description: pcscPage.description
            }

            Column {
                objectName: "detailsColumn"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                visible: pcscHandler.tagDetected

                DetailItem {
                    objectName: "attributeItem"
                    label: qsTr("Attribute:")
                    value: pcscHandler.attribute
                    alignment: Qt.AlignLeft | Qt.AlignVCenter
                }

                DetailItem {
                    objectName: "stateItem"
                    label: qsTr("State:")
                    value: pcscHandler.state
                    alignment: Qt.AlignLeft | Qt.AlignVCenter
                }

                DetailItem {
                    objectName: "checksumItem"
                    label: qsTr("Checksum:")
                    value: pcscHandler.checksum
                    alignment: Qt.AlignLeft | Qt.AlignVCenter
                }

                DetailItem {
                    objectName: "protocolItem"
                    label: qsTr("Protocol:")
                    value: pcscHandler.protocol
                    alignment: Qt.AlignLeft | Qt.AlignVCenter
                }
            }
        }
    }
}
