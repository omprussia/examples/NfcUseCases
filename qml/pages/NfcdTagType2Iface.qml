// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.NfcUseCases 1.0
import "../components"

Item {
    id: nfcdTagType2Iface

    property string path

    objectName: "nfcdTagType2Iface"

    NfcdTagType2Handler {
        id: nfcdTagType2Handler

        objectName: "nfcdTagType2Handler"
        tagPath: path
    }

    SilicaFlickable {
        objectName: "flickableArea"
        anchors {
            fill: parent
            bottomMargin: Theme.paddingMedium
        }
        contentHeight: mainColumn.implicitHeight
        clip: true

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: mainColumn

            objectName: "mainColumn"
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingMedium

            SectionHeader {
                objectName: "tagType2InfoSection"
                text: qsTr("Tag type 2 info")
            }

            AlignedLabelItem {
                label: qsTr("Interface version:")
                value: nfcdTagType2Handler.interfaceVersion
            }

            AlignedLabelItem {
                objectName: "blockSizeItem"
                label: qsTr("Block size:")
                value: nfcdTagType2Handler.blockSize
            }

            AlignedLabelItem {
                objectName: "dataSizeItem"
                label: qsTr("Data size:")
                value: nfcdTagType2Handler.dataSize
            }

            AlignedLabelItem {
                objectName: "serialItem"
                label: qsTr("Serial:")
                value: nfcdTagType2Handler.serial
            }

            SectionHeader {
                objectName: "funcWriteSection"
                text: qsTr("Function 'write'")
            }

            HexValueItem {
                id: writeSectorValueItem

                objectName: "writeSectorValueItem"
                label: qsTr("Sector")
                text: "0"
            }

            HexValueItem {
                id: writeBlockValueItem

                objectName: "writeBlockValueItem"
                label: qsTr("Block")
                text: "0"
            }

            HexValueItem {
                id: writeDataValueItem

                objectName: "writeDataValueItem"
                label: qsTr("Data")
                text: "0x0"
            }

            DetailButtonItem {
                objectName: "writeResultItem"
                buttonEnabled: !writeSectorValueItem.errorHighlight
                               || !writeBlockValueItem.errorHighlight
                               || !writeDataValueItem.errorHighlight

                buttonText: qsTr("Write")
                resultLabel: qsTr("Result:")

                onClicked: resultValue = nfcdTagType2Handler.write(writeSectorValueItem.text,
                                                                   writeBlockValueItem.text,
                                                                   writeDataValueItem.text)

            }

            SectionHeader {
                objectName: "funcWriteDataSection"
                text: qsTr("Function 'writeData'")
            }

            HexValueItem {
                id: writeDataOffsetValueItem

                objectName: "writeDataOffsetValueItem"
                label: qsTr("Offset")
                text: "0"
            }

            HexValueItem {
                id: writeDataDataValueItem

                objectName: "writeDataDataValueItem"
                label: qsTr("Data")
                text: "0x0"
            }

            DetailButtonItem {
                objectName: "writeDataResultItem"
                buttonEnabled: !writeDataOffsetValueItem.errorHighlight
                               || !writeDataDataValueItem.errorHighlight

                buttonText: qsTr("Write")
                resultLabel: qsTr("Result:")

                onClicked: resultValue = nfcdTagType2Handler.writeData(writeDataOffsetValueItem.text,
                                                                       writeDataDataValueItem.text)

            }

            SectionHeader {
                objectName: "funcReadSection"
                text: qsTr("Function 'read'")
            }

            HexValueItem {
                id: readSectorValueItem

                objectName: "readSectorValueItem"
                label: qsTr("Sector")
                text: "0"
            }

            HexValueItem {
                id: readBlockValueItem

                objectName: "readBlockValueItem"
                label: qsTr("Block")
                text: "0"
            }

            DetailButtonItem {
                objectName: "readResultItem"
                buttonEnabled: !readSectorValueItem.errorHighlight
                               || !readBlockValueItem.errorHighlight
                buttonText: qsTr("Read")
                resultLabel: qsTr("Result:")

                onClicked: resultValue = nfcdTagType2Handler.read(readSectorValueItem.text,
                                                                  readBlockValueItem.text)
            }

            SectionHeader {
                objectName: "funcReadDataSection"
                text: qsTr("Function 'readData'")
            }

            HexValueItem {
                id: readDataOffsetValueItem

                objectName: "readDataOffsetValueItem"
                label: qsTr("Offset")
                text: "0"
            }

            HexValueItem {
                id: readDataMaxBytesValueItem

                objectName: "readDataMaxBytesValueItem"
                label: qsTr("Max bytes")
                text: "0"
            }

            DetailButtonItem {
                objectName: "readDataResultItem"
                buttonEnabled: !readDataOffsetValueItem.errorHighlight
                               || !readDataMaxBytesValueItem.errorHighlight
                buttonText: qsTr("Read")
                resultLabel: qsTr("Result:")

                onClicked: resultValue = nfcdTagType2Handler.readData(readDataOffsetValueItem.text,
                                                                      readDataMaxBytesValueItem.text)
            }

            SectionHeader {
                objectName: "funcReadAllDataSection"
                text: qsTr("Function 'readAllData'")
            }

            DetailButtonItem {
                objectName: "readAllDataResultItem"
                buttonText: qsTr("Read")
                resultLabel: qsTr("Result:")

                onClicked: resultValue = nfcdTagType2Handler.readAllData()
            }
        }
    }
}
