// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0

TextField {
    objectName: "hexValueItem"
    anchors {
        left: parent.left
        right: parent.right
    }
    horizontalAlignment: Text.AlignLeft
    font.pixelSize: Theme.fontSizeSmall
    color: errorHighlight ? Theme.errorColor : palette.highlightColor
    inputMethodHints: Qt.ImhNoPredictiveText
    validator: RegExpValidator {
        objectName: "regExpValidator"
        regExp: /^((0[xX])?([0-9a-fA-F]+))$/
    }
}
