// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0

Item {
    id: root

    property alias buttonEnabled: button.enabled
    property alias buttonText: button.text
    property alias resultLabel: labelItem.label
    property alias resultValue: labelItem.value

    signal clicked

    objectName: "detailButtonItem"
    anchors {
        left: parent.left
        right: parent.right
        leftMargin: Theme.horizontalPageMargin
        rightMargin: Theme.horizontalPageMargin
    }
    height: button.height

    Button {
        id: button

        objectName: "button"
        anchors {
            top: parent.top
            left: parent.left
        }
        width: Theme.buttonWidthExtraSmall

        onClicked: root.clicked()
    }

    AlignedLabelItem {
        id: labelItem

        objectName: "labelItem"
        anchors {
            left: button.right
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
    }
}
