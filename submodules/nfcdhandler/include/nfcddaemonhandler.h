// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NFCDDAEMONHANDLER_H
#define NFCDDAEMONHANDLER_H

#include <QtCore/QAbstractListModel>
#include <QtCore/QSharedPointer>

#include "nfcdtypes.h"

class NfcdAdaptersModelPrivate;

class NfcdAdaptersModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit NfcdAdaptersModel(QObject *parent = nullptr) = delete;
    NfcdAdaptersModel(NfcdAdaptersModelPrivate *data, QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent) const override;

private:
    friend class NfcdAdaptersModelPrivate;
    QSharedPointer<NfcdAdaptersModelPrivate> m_data{ nullptr };
};

class NfcdDaemonHandlerPrivate;

class NfcdDaemonHandler : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString interfaceVersion READ interfaceVersion CONSTANT)
    Q_PROPERTY(QString daemonVersion READ daemonVersion CONSTANT)
    Q_PROPERTY(NfcdAdaptersModel *adaptersModel READ adaptersModel CONSTANT)

public:
    explicit NfcdDaemonHandler(QObject *parent = nullptr);

    QString interfaceVersion();
    QString daemonVersion();
    NfcdAdaptersModel *adaptersModel();

private:
    QSharedPointer<NfcdDaemonHandlerPrivate> m_data{ nullptr };
};

#endif // NFCDDAEMONHANDLER_H
