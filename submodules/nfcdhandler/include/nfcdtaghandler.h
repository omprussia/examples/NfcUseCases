// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NFCDTAGHANDLER_H
#define NFCDTAGHANDLER_H

#include <QtCore/QSharedPointer>
#include <QtCore/QVariantMap>

#include "nfcdtypes.h"

class NfcdTagHandlerPrivate;

class NfcdTagHandler : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString tagPath READ tagPath WRITE setTagPath NOTIFY tagPathChanged)
    Q_PROPERTY(QString interfaceVersion READ interfaceVersion NOTIFY interfaceVersionChanged)
    Q_PROPERTY(bool present READ present NOTIFY presentChanged)
    Q_PROPERTY(NfcdTagProtocol protocol READ protocol NOTIFY protocolChanged)
    Q_PROPERTY(NfcdTagTechnology technology READ technology NOTIFY technologyChanged)
    Q_PROPERTY(quint32 type READ type NOTIFY typeChanged)
    Q_PROPERTY(QStringList interfaces READ interfaces NOTIFY interfacesChanged)
    Q_PROPERTY(QStringList ndefRecords READ ndefRecords NOTIFY ndefRecordsChanged)
    Q_PROPERTY(QVariantMap pollParameters READ pollParameters NOTIFY pollParametersChanged)

public:
    explicit NfcdTagHandler(QObject *parent = nullptr);

    QString tagPath() const;
    void setTagPath(const QString &tagPath);

    QString interfaceVersion();
    bool present();
    NfcdTagProtocol protocol();
    NfcdTagTechnology technology();
    quint32 type();
    QStringList interfaces();
    QStringList ndefRecords();
    QVariantMap pollParameters();

    Q_INVOKABLE void acquire(bool wait);
    Q_INVOKABLE void deactivate();
    Q_INVOKABLE void release();
    Q_INVOKABLE QString transceive(const QString &data);

signals:
    void tagPathChanged(const QString &tagPath);
    void interfaceVersionChanged(const QString &interfaceVersion);
    void presentChanged(bool present);
    void protocolChanged(NfcdTagProtocol protocol);
    void technologyChanged(NfcdTagTechnology technology);
    void typeChanged(quint32 type);
    void interfacesChanged(const QStringList &interfaces);
    void ndefRecordsChanged(const QStringList &ndefRecords);
    void pollParametersChanged(const QVariantMap &pollParameters);
    void removed();

private:
    QSharedPointer<NfcdTagHandlerPrivate> m_data{ nullptr };
};

#endif // NFCDTAGHANDLER_H
