# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

QT += core dbus

INCLUDEPATH += \
    $$PWD/include \

DEPENDPATH += \
    $$PWD/include \

HEADERS_PUBLIC = \
    $$PWD/include/nfcdtypes.h \
    $$PWD/include/nfcddaemonhandler.h \
    $$PWD/include/nfcdadapterhandler.h \
    $$PWD/include/nfcdtaghandler.h \
    $$PWD/include/nfcdtagtype2handler.h \
    $$PWD/include/nfcdisodephandler.h \
    $$PWD/include/nfcdndefhandler.h \

HEADERS_PRIVATE = \
    $$PWD/src/nfcddaemonhandler_p.h \
    $$PWD/src/nfcdadapterhandler_p.h \
    $$PWD/src/nfcdtaghandler_p.h \
    $$PWD/src/nfcdtagtype2handler_p.h \
    $$PWD/src/nfcdisodephandler_p.h \
    $$PWD/src/nfcdndefhandler_p.h \

HEADERS += \
    $$HEADERS_PUBLIC \
    $$HEADERS_PRIVATE \

SOURCES += \
    $$PWD/src/nfcddaemonhandler.cpp \
    $$PWD/src/nfcdadapterhandler.cpp \
    $$PWD/src/nfcdtaghandler.cpp \
    $$PWD/src/nfcdtagtype2handler.cpp \
    $$PWD/src/nfcdisodephandler.cpp \
    $$PWD/src/nfcdndefhandler.cpp \

include ($$PWD/src/dbus/nfcddbusifaces.pri)
