# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

QT += core dbus

INCLUDEPATH += \
    $$PWD \

HEADERS += \
    $$PWD/nfcdconstants.h \
    $$PWD/nfcddaemondbusiface.h \
    $$PWD/nfcdadapterdbusiface.h \
    $$PWD/nfcdtagdbusiface.h \
    $$PWD/nfcdtagtype2dbusiface.h \
    $$PWD/nfcdisodepdbusiface.h \
    $$PWD/nfcdndefdbusiface.h \

SOURCES += \
    $$PWD/nfcdconstants.cpp \
    $$PWD/nfcdadapterdbusiface.cpp \
    $$PWD/nfcddaemondbusiface.cpp \
    $$PWD/nfcdtagdbusiface.cpp \
    $$PWD/nfcdtagtype2dbusiface.cpp \
    $$PWD/nfcdisodepdbusiface.cpp \
    $$PWD/nfcdndefdbusiface.cpp \
