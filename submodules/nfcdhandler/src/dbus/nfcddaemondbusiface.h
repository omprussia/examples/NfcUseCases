// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NFCDDAEMONDBUSIFACE_H
#define NFCDDAEMONDBUSIFACE_H

#include <QtCore/QList>
#include <QtDBus/QDBusAbstractInterface>
#include <QtDBus/QDBusPendingReply>

class NfcdDaemonDBusIface : public QDBusAbstractInterface
{
    Q_OBJECT

public:
    NfcdDaemonDBusIface(QObject *parent = nullptr);

public slots:
    QDBusPendingReply<int> GetInterfaceVersion();
    QDBusPendingReply<int> GetDaemonVersion();
    QDBusPendingReply<QList<QDBusObjectPath>> GetAdapters();

signals:
    void AdaptersChanged(const QList<QDBusObjectPath> &adapters);
};

#endif // NFCDDAEMONDBUSIFACE_H
