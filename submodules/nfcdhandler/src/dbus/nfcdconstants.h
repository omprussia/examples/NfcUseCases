// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NFCDCONSTANTS_H
#define NFCDCONSTANTS_H

#include <QtCore/QString>

class NfcdConstants
{
public:
    static const QString nfcdService;
    static const QString nfcdPath;

    static const char *nfcdDaemonIface;
    static const char *nfcdAdapterIface;
    static const char *nfcdIsoDepIface;
    static const char *nfcdNdefIface;
    static const char *nfcdTagIface;
    static const char *nfcdTagType2Iface;

private:
    explicit NfcdConstants() = delete;
    ~NfcdConstants() = default;
};

#endif // NFCDCONSTANTS_P_H
