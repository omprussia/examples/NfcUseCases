// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "nfcdconstants.h"
#include "nfcddaemondbusiface.h"

NfcdDaemonDBusIface::NfcdDaemonDBusIface(QObject *parent)
    : QDBusAbstractInterface(NfcdConstants::nfcdService, NfcdConstants::nfcdPath,
                             NfcdConstants::nfcdDaemonIface, QDBusConnection::systemBus(), parent)
{
}

QDBusPendingReply<int> NfcdDaemonDBusIface::GetInterfaceVersion()
{
    return asyncCallWithArgumentList(QStringLiteral("GetInterfaceVersion"), QList<QVariant>());
}

QDBusPendingReply<int> NfcdDaemonDBusIface::GetDaemonVersion()
{
    return asyncCallWithArgumentList(QStringLiteral("GetDaemonVersion"), QList<QVariant>());
}

QDBusPendingReply<QList<QDBusObjectPath>> NfcdDaemonDBusIface::GetAdapters()
{
    return asyncCallWithArgumentList(QStringLiteral("GetAdapters"), QList<QVariant>());
}
