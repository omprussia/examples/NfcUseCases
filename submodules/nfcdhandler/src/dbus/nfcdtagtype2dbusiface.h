// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NFCDTAGTYPE2DBUSIFACE_H
#define NFCDTAGTYPE2DBUSIFACE_H

#include <QtCore/QList>
#include <QtDBus/QDBusAbstractInterface>
#include <QtDBus/QDBusPendingReply>

class NfcdTagType2DBusIface : public QDBusAbstractInterface
{
    Q_OBJECT

public:
    NfcdTagType2DBusIface(const QString &path, QObject *parent = nullptr);

public slots:
    QDBusPendingReply<int> GetInterfaceVersion();
    QDBusPendingReply<uint> GetBlockSize();
    QDBusPendingReply<uint> GetDataSize();
    QDBusPendingReply<uint> Write(uint sector, uint block, const QByteArray &data);
    QDBusPendingReply<uint> WriteData(uint offset, const QByteArray &data);
    QDBusPendingReply<QByteArray> Read(uint sector, uint block);
    QDBusPendingReply<QByteArray> ReadData(uint offset, uint maxbytes);
    QDBusPendingReply<QByteArray> ReadAllData();
    QDBusPendingReply<QByteArray> GetSerial();
};

#endif // NFCDTAGTYPE2DBUSIFACE_H
