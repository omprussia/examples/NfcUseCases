// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "nfcdconstants.h"
#include "nfcdadapterdbusiface.h"

NfcdAdapterDBusIface::NfcdAdapterDBusIface(const QString &path, QObject *parent)
    : QDBusAbstractInterface(NfcdConstants::nfcdService, path, NfcdConstants::nfcdAdapterIface,
                             QDBusConnection::systemBus(), parent)
{
}

QDBusPendingReply<int> NfcdAdapterDBusIface::GetInterfaceVersion()
{
    return asyncCallWithArgumentList(QStringLiteral("GetInterfaceVersion"), QList<QVariant>());
}

QDBusPendingReply<bool> NfcdAdapterDBusIface::GetEnabled()
{
    return asyncCallWithArgumentList(QStringLiteral("GetEnabled"), QList<QVariant>());
}

QDBusPendingReply<bool> NfcdAdapterDBusIface::GetPowered()
{
    return asyncCallWithArgumentList(QStringLiteral("GetPowered"), QList<QVariant>());
}

QDBusPendingReply<bool> NfcdAdapterDBusIface::GetTargetPresent()
{
    return asyncCallWithArgumentList(QStringLiteral("GetTargetPresent"), QList<QVariant>());
}

QDBusPendingReply<uint> NfcdAdapterDBusIface::GetSupportedModes()
{
    return asyncCallWithArgumentList(QStringLiteral("GetSupportedModes"), QList<QVariant>());
}

QDBusPendingReply<uint> NfcdAdapterDBusIface::GetMode()
{
    return asyncCallWithArgumentList(QStringLiteral("GetMode"), QList<QVariant>());
}

QDBusPendingReply<QList<QDBusObjectPath>> NfcdAdapterDBusIface::GetTags()
{
    return asyncCallWithArgumentList(QStringLiteral("GetTags"), QList<QVariant>());
}
