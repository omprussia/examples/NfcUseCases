// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef NFCDADAPTERDBUSIFACE_H
#define NFCDADAPTERDBUSIFACE_H

#include <QtCore/QList>
#include <QtDBus/QDBusAbstractInterface>
#include <QtDBus/QDBusPendingReply>

class NfcdAdapterDBusIface : public QDBusAbstractInterface
{
    Q_OBJECT

public:
    NfcdAdapterDBusIface(const QString &path, QObject *parent = nullptr);

public slots:
    QDBusPendingReply<int> GetInterfaceVersion();
    QDBusPendingReply<bool> GetEnabled();
    QDBusPendingReply<bool> GetPowered();
    QDBusPendingReply<bool> GetTargetPresent();
    QDBusPendingReply<uint> GetSupportedModes();
    QDBusPendingReply<uint> GetMode();
    QDBusPendingReply<QList<QDBusObjectPath>> GetTags();

signals:
    void EnabledChanged(bool enabled);
    void PoweredChanged(bool powered);
    void TargetPresentChanged(bool targetPresent);
    void ModeChanged(uint mode);
    void TagsChanged(const QList<QDBusObjectPath> &tags);
};

#endif // NFCDADAPTERDBUSIFACE_H
