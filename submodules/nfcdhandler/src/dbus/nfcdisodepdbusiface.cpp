// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include "nfcdconstants.h"
#include "nfcdisodepdbusiface.h"

NfcdIsoDepDBusIface::NfcdIsoDepDBusIface(const QString &path, QObject *parent)
    : QDBusAbstractInterface(NfcdConstants::nfcdService, path, NfcdConstants::nfcdIsoDepIface,
                             QDBusConnection::systemBus(), parent)
{
}

QDBusPendingReply<int> NfcdIsoDepDBusIface::GetInterfaceVersion()
{
    return asyncCallWithArgumentList(QStringLiteral("GetInterfaceVersion"), QList<QVariant>());
}

QDBusPendingReply<QByteArray, uchar, uchar> NfcdIsoDepDBusIface::Transmit(uchar cla, uchar ins,
                                                                          uchar p1, uchar p2,
                                                                          const QByteArray &data,
                                                                          uint le)
{
    QList<QVariant> arguments = {
        QVariant::fromValue(cla), QVariant::fromValue(ins),  QVariant::fromValue(p1),
        QVariant::fromValue(p2),  QVariant::fromValue(data), QVariant::fromValue(le),
    };

    return asyncCallWithArgumentList(QStringLiteral("Transmit"), arguments);
}

QDBusPendingReply<> NfcdIsoDepDBusIface::Reset()
{
    return asyncCallWithArgumentList(QStringLiteral("Reset"), QList<QVariant>());
}
