// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include "nfcdconstants.h"
#include "nfcdndefdbusiface.h"

NfcdNdefDBusIface::NfcdNdefDBusIface(const QString &path, QObject *parent)
    : QDBusAbstractInterface(NfcdConstants::nfcdService, path, NfcdConstants::nfcdNdefIface,
                             QDBusConnection::systemBus(), parent)
{
}

QDBusPendingReply<int> NfcdNdefDBusIface::GetInterfaceVersion()
{
    return asyncCallWithArgumentList(QStringLiteral("GetInterfaceVersion"), QList<QVariant>());
}

QDBusPendingReply<uint> NfcdNdefDBusIface::GetFlags()
{
    return asyncCallWithArgumentList(QStringLiteral("GetFlags"), QList<QVariant>());
}

QDBusPendingReply<uint> NfcdNdefDBusIface::GetTypeNameFormat()
{
    return asyncCallWithArgumentList(QStringLiteral("GetTypeNameFormat"), QList<QVariant>());
}

QDBusPendingReply<QByteArray> NfcdNdefDBusIface::GetType()
{
    return asyncCallWithArgumentList(QStringLiteral("GetType"), QList<QVariant>());
}

QDBusPendingReply<QByteArray> NfcdNdefDBusIface::GetId()
{
    return asyncCallWithArgumentList(QStringLiteral("GetId"), QList<QVariant>());
}

QDBusPendingReply<QByteArray> NfcdNdefDBusIface::GetPayload()
{
    return asyncCallWithArgumentList(QStringLiteral("GetPayload"), QList<QVariant>());
}

QDBusPendingReply<QByteArray> NfcdNdefDBusIface::GetRawData()
{
    return asyncCallWithArgumentList(QStringLiteral("GetRawData"), QList<QVariant>());
}

QDBusPendingReply<QStringList> NfcdNdefDBusIface::GetInterfaces()
{
    return asyncCallWithArgumentList(QStringLiteral("GetInterfaces"), QList<QVariant>());
}
