// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NFCDISODEPHANDLER_P_H
#define NFCDISODEPHANDLER_P_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

#include "nfcdtypes.h"
#include "nfcdisodepdbusiface.h"

class NfcdIsoDepHandlerPrivate : public QObject
{
    Q_OBJECT

public:
    explicit NfcdIsoDepHandlerPrivate(QObject *parent = nullptr);

    QString tagPath() const;
    void setTagPath(const QString &tagPath);

    QString interfaceVersion();
    NfcdIsoDepResult transmit(uchar cla, uchar ins, uchar p1, uchar p2, const QString &data,
                              uint le);
    void reset();

signals:
    void tagPathChanged(const QString &tagPath);
    void interfaceVersionChanged(const QString &interfaceVersion);

private:
    QSharedPointer<NfcdIsoDepDBusIface> m_iface{ nullptr };
    QString m_tagPath{};
};

#endif // NFCDISODEPHANDLER_P_H
