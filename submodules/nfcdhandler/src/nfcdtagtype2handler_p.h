// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NFCDTAGTYPE2HANDLER_P_H
#define NFCDTAGTYPE2HANDLER_P_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

#include "nfcdtypes.h"
#include "nfcdtagtype2dbusiface.h"

class NfcdTagType2HandlerPrivate : public QObject
{
    Q_OBJECT

public:
    explicit NfcdTagType2HandlerPrivate(QObject *parent = nullptr);

    QString tagPath() const;
    void setTagPath(const QString &tagPath);

    QString interfaceVersion();
    quint32 blockSize();
    quint32 dataSize();
    quint32 write(quint32 sector, quint32 block, const QString &data);
    quint32 writeData(quint32 offset, const QString &data);
    QString read(quint32 sector, quint32 block);
    QString readData(quint32 offset, quint32 maxbytes);
    QString readAllData();
    QString serial();

signals:
    void tagPathChanged(const QString &tagPath);
    void interfaceVersionChanged(const QString &interfaceVersion);
    void blockSizeChanged(quint32 blockSize);
    void dataSizeChanged(quint32 dataSize);
    void serialChanged(const QString &serial);

private:
    QSharedPointer<NfcdTagType2DBusIface> m_iface{ nullptr };
    QString m_tagPath{};
};

#endif // NFCDTAGTYPE2HANDLER_P_H
