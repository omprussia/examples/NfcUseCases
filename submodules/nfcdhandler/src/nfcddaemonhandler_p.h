// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NFCDDAEMONHANDLER_P_H
#define NFCDDAEMONHANDLER_P_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

#include "nfcdtypes.h"
#include "nfcddaemondbusiface.h"
#include "nfcdadapterdbusiface.h"

class NfcdAdaptersModel;

class NfcdAdaptersModelPrivate : public QObject
{
    Q_OBJECT

public:
    enum Role {
        PathRole = Qt::UserRole + 1,
        EnabledRole,
        PoweredRole,
    };

public:
    NfcdAdaptersModelPrivate(NfcdDaemonDBusIface *iface);

    void setParent(NfcdAdaptersModel *parent);

    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent) const;

private slots:
    void updateAdapters(const QList<QDBusObjectPath> &adapters);

private:
    NfcdAdaptersModel *m_parent{ nullptr };
    NfcdDaemonDBusIface *m_daemon{ nullptr };
    QMap<QString, QSharedPointer<NfcdAdapterDBusIface>> m_adapters{};
    QHash<int, QByteArray> m_roleNames{};
};

class NfcdDaemonHandlerPrivate : public QObject
{
    Q_OBJECT

public:
    explicit NfcdDaemonHandlerPrivate(QObject *parent = nullptr);

    QString interfaceVersion();
    QString daemonVersion();
    NfcdAdaptersModel *adaptersModel();

private:
    QSharedPointer<NfcdDaemonDBusIface> m_iface{ nullptr };
    QSharedPointer<NfcdAdaptersModel> m_model{ nullptr };
};

#endif // NFCDDAEMONHANDLER_P_H
