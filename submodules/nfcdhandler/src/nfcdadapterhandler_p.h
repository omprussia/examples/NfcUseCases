// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NFCDADAPTERHANDLER_P_H
#define NFCDADAPTERHANDLER_P_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

#include "nfcdtypes.h"
#include "nfcdtagdbusiface.h"
#include "nfcdadapterdbusiface.h"

class NfcdTagsModel;

class NfcdTagsModelPrivate : public QObject
{
    Q_OBJECT

public:
    enum Role {
        PathRole = Qt::UserRole + 1,
        ProtocolRole,
        TechnologyRole,
        TypeRole,
    };

public:
    NfcdTagsModelPrivate(NfcdAdapterDBusIface *iface);

    void setParent(NfcdTagsModel *parent);

    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent) const;

private slots:
    void updateTags(const QList<QDBusObjectPath> &tags);

private:
    NfcdTagsModel *m_parent{ nullptr };
    NfcdAdapterDBusIface *m_adapter{ nullptr };
    QMap<QString, QSharedPointer<NfcdTagDBusIface>> m_tags{};
    QHash<int, QByteArray> m_roleNames{};
};

class NfcdAdapterHandlerPrivate : public QObject
{
    Q_OBJECT

public:
    explicit NfcdAdapterHandlerPrivate(QObject *parent = nullptr);

    QString adapterPath() const;
    void setAdapterPath(const QString &adapterPath);

    QString interfaceVersion();
    bool enabled();
    bool powered();
    bool targetPresent();
    NfcdModes supportedModes();
    NfcdMode mode();
    NfcdTagsModel *tagsModel();

signals:
    void adapterPathChanged(const QString &adapterPath);
    void interfaceVersionChanged(const QString &interfaceVersion);
    void enabledChanged(bool enabled);
    void poweredChanged(bool powered);
    void targetPresentChanged(bool targetPresent);
    void supportedModesChanged(NfcdModes supportedModes);
    void modeChanged(NfcdMode mode);
    void tagsModelChanged(NfcdTagsModel *tagsModel);

private:
    QSharedPointer<NfcdAdapterDBusIface> m_iface{ nullptr };
    QSharedPointer<NfcdTagsModel> m_model{ nullptr };
    QString m_adapterPath{};
};

#endif // NFCDADAPTERHANDLER_P_H
