# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

QT += core concurrent network

PKGCONFIG += \
    libpcsclite \

INCLUDEPATH += \
    $$PWD/include \

DEPENDPATH += \
    $$PWD/include \

HEADERS_PUBLIC = \
    $$PWD/include/pcschandler.h \

HEADERS_PRIVATE = \
    $$PWD/src/pcschandler_p.h \

HEADERS += \
    $$HEADERS_PUBLIC \
    $$HEADERS_PRIVATE \

SOURCES += \
    $$PWD/src/pcschandler.cpp \
