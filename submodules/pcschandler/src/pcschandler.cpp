// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtCore/QDir>
#include <QtCore/QStandardPaths>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <QtConcurrent/QtConcurrent>

#include "pcschandler_p.h"
#include "pcschandler.h"

struct SCardStatusData
{
    DWORD dwState{ 0 };
    DWORD dwProt{ 0 };
    BYTE pbAtr[MAX_ATR_SIZE]{ '\0' };
    DWORD dwAtrLen{ sizeof(pbAtr) };
};

PcscHandlerPrivate::PcscHandlerPrivate(QObject *parent) : QObject(parent)
{
    _initialize();
}

PcscHandlerPrivate::~PcscHandlerPrivate()
{
    _deinitialize();
}

bool PcscHandlerPrivate::tagDetected() const
{
    return m_tagDetected;
}

QString PcscHandlerPrivate::attribute() const
{
    return m_attribute;
}

QString PcscHandlerPrivate::state() const
{
    return m_state;
}

QString PcscHandlerPrivate::checksum() const
{
    return m_checksum;
}

QString PcscHandlerPrivate::protocol() const
{
    return m_protocol;
}

void PcscHandlerPrivate::_initialize()
{
    const QDir dir = QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    const QString fileNameTamplate = QStringLiteral("SmartCardList_%1");
    const QString fileNameDateNew = QDate::currentDate().toString(QStringLiteral("dd_MM_yyyy"));
    const QString fileNameNew = fileNameTamplate.arg(fileNameDateNew);
    QString fileNameDateOld = QStringLiteral("");
    QString fileNameOld = QStringLiteral("");

    m_manager.reset(new QNetworkAccessManager);
    connect(m_manager.data(), &QNetworkAccessManager::finished, [=](QNetworkReply *reply) {
        if (reply->error() != QNetworkReply::NoError)
            return;

        QFile file(dir.absoluteFilePath(fileNameNew));
        if (file.open(QFile::WriteOnly | QFile::Truncate | QFile::Text)) {
            file.write(reply->readAll());
            file.close();
        }

        _parseAttributes(dir.absoluteFilePath(fileNameNew));
        _updateAttribute();
    });

    for (const auto &file :
         dir.entryInfoList({ fileNameTamplate.arg(QStringLiteral("*")) }, QDir::Files)) {
        fileNameOld = file.fileName();
        fileNameDateOld = fileNameOld;
        fileNameDateOld = fileNameDateOld.remove(fileNameTamplate.arg(QStringLiteral("")));
        break;
    }

    if (QFile::exists(dir.absoluteFilePath(fileNameOld))
        && fileNameDateOld.compare(fileNameDateNew) != 0)
        QFile::remove(dir.absoluteFilePath(fileNameOld));

    if (QFile::exists(dir.absoluteFilePath(fileNameNew)))
        _parseAttributes(dir.absoluteFilePath(fileNameNew));
    else
        m_manager->get(QNetworkRequest(QStringLiteral(
                "http://ludovic.rousseau.free.fr/softwares/pcsc-tools/smartcard_list.txt")));

    LONG rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, nullptr, nullptr, &m_sCardContext);
    if (rv != SCARD_S_SUCCESS) {
        _deinitialize();
        return;
    }

    _detectReaders();
    m_events = QtConcurrent::run(this, &PcscHandlerPrivate::_detectEvents);
}

void PcscHandlerPrivate::_deinitialize()
{
    m_working = 0;
    if (m_events.isRunning()) {
        m_events.cancel();
        m_events.waitForFinished();
    }

    SCardReleaseContext(m_sCardContext);
}

void PcscHandlerPrivate::_parseAttributes(const QString &filePath)
{
    m_attributes.clear();
    m_attributesNonStandard.clear();

    QFile file(filePath);
    if (!file.open(QFile::ReadOnly))
        return;

    QTextStream stream(&file);
    QString line;
    QString key;
    QStringList value;
    while (stream.readLineInto(&line)) {
        line = line.simplified();

        if (line.startsWith(QStringLiteral("#")))
            continue;

        if (line.isEmpty()) {
            if (!key.isEmpty() || !value.isEmpty()) {
                bool nonStandard = key.count(QStringLiteral(".")) > 0
                        || key.count(QStringLiteral("[")) > 0 || key.count(QStringLiteral("]")) > 0;
                nonStandard ? m_attributesNonStandard.insert(key, value)
                            : m_attributes.insert(key, value);
                key.clear();
                value.clear();
            }
        } else {
            if (key.isEmpty())
                key = line;
            else
                value.append(line);
        }
    }
}

void PcscHandlerPrivate::_detectReaders()
{
    LPSTR mszReaders = nullptr;
    DWORD dwReaders = SCARD_AUTOALLOCATE;
    LONG rv = SCardListReaders(m_sCardContext, nullptr, (LPSTR)&mszReaders, &dwReaders);
    if (rv != SCARD_S_SUCCESS) {
        if (mszReaders)
            SCardFreeMemory(m_sCardContext, mszReaders);
        _deinitialize();
        return;
    }

    m_readers.clear();
    char *ptr = mszReaders;
    while (*ptr != '\0') {
        m_readers.append(ptr);
        ptr += strlen(ptr) + 1;
    }
}

void PcscHandlerPrivate::_detectEvents()
{
    SCARD_READERSTATE readerState = { 0 };
    readerState.szReader = (LPCTSTR)m_readers.first();
    readerState.dwCurrentState = SCARD_STATE_UNAWARE;
    readerState.dwEventState = SCARD_STATE_UNKNOWN;

    LONG rv = SCardGetStatusChange(m_sCardContext, INFINITE, &readerState, 1);
    bool firstAttempt = true;

    while (m_working) {
        if ((readerState.dwEventState & SCARD_STATE_EMPTY) == SCARD_STATE_EMPTY) {
            if (firstAttempt)
                firstAttempt = false;
            else
                _processDisconnect();
        } else if ((readerState.dwEventState & SCARD_STATE_PRESENT) == SCARD_STATE_PRESENT) {
            _processConnect();
        }

        bool needContinue = false;
        do {
            readerState.dwCurrentState = readerState.dwEventState;
            rv = SCardGetStatusChange(m_sCardContext, 50, &readerState, 1);

            DWORD stateMask = ~(SCARD_STATE_INUSE | SCARD_STATE_EXCLUSIVE | SCARD_STATE_UNAWARE
                                | SCARD_STATE_IGNORE | SCARD_STATE_CHANGED);
            DWORD stateNew = readerState.dwEventState & stateMask;
            DWORD stateOld = readerState.dwCurrentState & stateMask;
            if (stateNew != stateOld)
                needContinue = true;
        } while (!needContinue && m_working);
    }
}

void PcscHandlerPrivate::_processConnect()
{
    SCARDHANDLE hCard = 0;
    DWORD dwActiveProtocol = -1;
    LONG rv = SCardConnect(m_sCardContext, (LPCTSTR)m_readers.first(), SCARD_SHARE_SHARED,
                           SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &hCard, &dwActiveProtocol);
    if (rv != SCARD_S_SUCCESS)
        return;

    char pbReader[MAX_READERNAME] = "";
    DWORD dwReaderLen = sizeof(pbReader);

    m_sCardStatusData.reset(new SCardStatusData());
    rv = SCardStatus(hCard, pbReader, &dwReaderLen, &m_sCardStatusData->dwState,
                     &m_sCardStatusData->dwProt, m_sCardStatusData->pbAtr,
                     &m_sCardStatusData->dwAtrLen);
    if (rv != SCARD_S_SUCCESS)
        return;

    m_tagDetected = true;
    emit tagDetectedChanged(m_tagDetected);

    _updateAttribute();
    _updateChecksum();
    _updateState();
    _updateProtocol();
}

void PcscHandlerPrivate::_processDisconnect()
{
    m_sCardStatusData.reset();

    m_tagDetected = false;
    emit tagDetectedChanged(m_tagDetected);

    _updateAttribute();
    _updateChecksum();
    _updateState();
    _updateProtocol();
}

void PcscHandlerPrivate::_updateAttribute()
{
    QString attribute;
    if (!m_sCardStatusData.isNull()) {
        for (DWORD i = 0; i < m_sCardStatusData->dwAtrLen; ++i)
            attribute += (i > 0 ? QStringLiteral(" ") : QStringLiteral(""))
                    + QString().sprintf("%02X", m_sCardStatusData->pbAtr[i]).toUpper();

        if (m_attributes.contains(attribute))
            attribute += QStringLiteral("\n · %1").arg(m_attributes.value(attribute).join("\n · "));
    }

    if (m_attribute != attribute) {
        m_attribute = attribute;
        emit attributeChanged(m_attribute);
    }
}

void PcscHandlerPrivate::_updateChecksum()
{
    QString checksum;
    if (!m_sCardStatusData.isNull()) {
        quint16 sum = 0;
        quint16 lastAtrByte = 0;
        for (DWORD i = 0; i < m_sCardStatusData->dwAtrLen; ++i) {
            sum = i > 0 ? m_sCardStatusData->pbAtr[m_sCardStatusData->dwAtrLen]
                            ^ quint16(m_sCardStatusData->pbAtr[i])
                        : 0;
            lastAtrByte = quint8(m_sCardStatusData->pbAtr[i]);
        }
        checksum =
                lastAtrByte == sum && sum > 0 ? QStringLiteral("valid") : QStringLiteral("invalid");
    }

    if (m_checksum != checksum) {
        m_checksum = checksum;
        emit checksumChanged(m_checksum);
    }
}

void PcscHandlerPrivate::_updateState()
{
    QString state;
    if (!m_sCardStatusData.isNull()) {
        QStringList states;

        if ((m_sCardStatusData->dwState & SCARD_ABSENT) == SCARD_ABSENT)
            states << QStringLiteral("absent");
        if ((m_sCardStatusData->dwState & SCARD_PRESENT) == SCARD_PRESENT)
            states << QStringLiteral("present");
        if ((m_sCardStatusData->dwState & SCARD_POWERED) == SCARD_POWERED)
            states << QStringLiteral("powered");
        if ((m_sCardStatusData->dwState & SCARD_SPECIFIC) == SCARD_SPECIFIC)
            states << QStringLiteral("specific");
        if ((m_sCardStatusData->dwState & SCARD_SWALLOWED) == SCARD_SWALLOWED)
            states << QStringLiteral("swallowed");
        if ((m_sCardStatusData->dwState & SCARD_NEGOTIABLE) == SCARD_NEGOTIABLE)
            states << QStringLiteral("negotiable");
        state = QStringLiteral("0x%1 (%2)")
                        .arg(QString::number(static_cast<quint32>(m_sCardStatusData->dwState), 16)
                                     .toUpper())
                        .arg(states.join(QStringLiteral(" | ")));
    }

    if (m_state != state) {
        m_state = state;
        emit stateChanged(m_state);
    }
}

void PcscHandlerPrivate::_updateProtocol()
{
    QString protocol;
    if (!m_sCardStatusData.isNull()) {
        switch (m_sCardStatusData->dwProt) {
        case SCARD_PROTOCOL_RAW:
            protocol = QStringLiteral("RAW");
            break;
        case SCARD_PROTOCOL_T0:
            protocol = QStringLiteral("T0");
            break;
        case SCARD_PROTOCOL_T1:
            protocol = QStringLiteral("T1");
            break;
        default:
            protocol = QStringLiteral("unknown or unexpected card protocol");
            break;
        }
    }

    if (m_protocol != protocol) {
        m_protocol = protocol;
        emit protocolChanged(m_protocol);
    }
}

PcscHandler::PcscHandler(QObject *parent) : QObject(parent), m_data(new PcscHandlerPrivate(this))
{
    connect(m_data.data(), &PcscHandlerPrivate::tagDetectedChanged, this,
            &PcscHandler::tagDetectedChanged, Qt::QueuedConnection);
    connect(m_data.data(), &PcscHandlerPrivate::attributeChanged, this,
            &PcscHandler::attributeChanged, Qt::QueuedConnection);
    connect(m_data.data(), &PcscHandlerPrivate::stateChanged, this, &PcscHandler::stateChanged,
            Qt::QueuedConnection);
    connect(m_data.data(), &PcscHandlerPrivate::checksumChanged, this,
            &PcscHandler::checksumChanged, Qt::QueuedConnection);
    connect(m_data.data(), &PcscHandlerPrivate::protocolChanged, this,
            &PcscHandler::protocolChanged, Qt::QueuedConnection);
}

bool PcscHandler::tagDetected() const
{
    return m_data->tagDetected();
}

QString PcscHandler::attribute() const
{
    return m_data->attribute();
}

QString PcscHandler::state() const
{
    return m_data->state();
}

QString PcscHandler::checksum() const
{
    return m_data->checksum();
}

QString PcscHandler::protocol() const
{
    return m_data->protocol();
}
