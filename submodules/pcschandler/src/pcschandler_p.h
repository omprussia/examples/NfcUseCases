// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PCSCHANDLER_P_H
#define PCSCHANDLER_P_H

#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QList>
#include <QtCore/QFuture>
#include <QtCore/QAtomicInt>
#include <QtCore/QSharedPointer>

#include <stdio.h>
#include <stdlib.h>
#include <winscard.h>
#include <wintypes.h>

using SCardContext = SCARDCONTEXT;

struct SCardStatusData;
class QNetworkAccessManager;
class PcscHandler;

class PcscHandlerPrivate : public QObject
{
    Q_OBJECT

public:
    explicit PcscHandlerPrivate(QObject *parent = nullptr);
    ~PcscHandlerPrivate() override;

    bool tagDetected() const;
    QString attribute() const;
    QString state() const;
    QString checksum() const;
    QString protocol() const;

signals:
    void tagDetectedChanged(bool tagDetected);
    void attributeChanged(const QString &attribute);
    void stateChanged(const QString &state);
    void checksumChanged(const QString &checksum);
    void protocolChanged(const QString &protocol);

private slots:
    void _initialize();
    void _deinitialize();
    void _parseAttributes(const QString &filePath);
    void _detectReaders();
    void _detectEvents();
    void _processConnect();
    void _processDisconnect();
    void _updateAttribute();
    void _updateChecksum();
    void _updateState();
    void _updateProtocol();

private:
    QSharedPointer<QNetworkAccessManager> m_manager{ nullptr };
    QSharedPointer<SCardStatusData> m_sCardStatusData{ nullptr };
    SCardContext m_sCardContext;
    QList<char *> m_readers{};
    QFuture<void> m_events{};
    QAtomicInt m_working{ 1 };
    QMap<QString, QStringList> m_attributes{};
    QMap<QString, QStringList> m_attributesNonStandard{};

    bool m_tagDetected{ false };
    QString m_attribute{};
    QString m_state{};
    QString m_checksum{};
    QString m_protocol{};
};

#endif // PCSCHANDLER_P_H
