// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PCSCHANDLER_H
#define PCSCHANDLER_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

class PcscHandlerPrivate;

class PcscHandler : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool tagDetected READ tagDetected NOTIFY tagDetectedChanged)
    Q_PROPERTY(QString attribute READ attribute NOTIFY attributeChanged)
    Q_PROPERTY(QString state READ state NOTIFY stateChanged)
    Q_PROPERTY(QString checksum READ checksum NOTIFY checksumChanged)
    Q_PROPERTY(QString protocol READ protocol NOTIFY protocolChanged)

public:
    explicit PcscHandler(QObject *parent = nullptr);

    bool tagDetected() const;
    QString attribute() const;
    QString state() const;
    QString checksum() const;
    QString protocol() const;

signals:
    void tagDetectedChanged(bool tagDetected);
    void attributeChanged(const QString &attribute);
    void stateChanged(const QString &state);
    void checksumChanged(const QString &checksum);
    void protocolChanged(const QString &protocol);

private:
    QSharedPointer<PcscHandlerPrivate> m_data{ nullptr };
};

#endif // PCSCHANDLER_H
