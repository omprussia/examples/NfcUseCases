Name:    ru.auroraos.NfcUseCases
Summary: NFC Use Cases
Version: 0.1.0
Release: 1
License: BSD-3-Clause
URL:     https://developer.auroraos.ru/open-source
Source0: %{name}-%{version}.tar.bz2

BuildRequires: pkgconfig(auroraapp)
BuildRequires: pkgconfig(Qt5Core)
BuildRequires: pkgconfig(Qt5Qml)
BuildRequires: pkgconfig(Qt5Quick)
BuildRequires: pkgconfig(Qt5DBus)
BuildRequires: pkgconfig(Qt5Network)
BuildRequires: pkgconfig(Qt5Concurrent)
BuildRequires: pkgconfig(libpcsclite)
Requires: sailfishsilica-qt5 >= 0.10.9

%description
The project provides an example of working with NFC stack.

%prep
%autosetup

%build
%qmake5
%make_build

%install
%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%defattr(644,root,root,-)
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
