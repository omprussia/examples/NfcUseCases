// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtGui/QGuiApplication>
#include <QtQuick/QQuickView>
#include <auroraapp.h>

#include "pcschandler.h"
#include "nfcddaemonhandler.h"
#include "nfcdadapterhandler.h"
#include "nfcdtaghandler.h"
#include "nfcdtagtype2handler.h"
#include "nfcdisodephandler.h"
#include "nfcdndefhandler.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<PcscHandler>("ru.auroraos.NfcUseCases", 1, 0, "PcscHandler");
    qmlRegisterType<NfcdDaemonHandler>("ru.auroraos.NfcUseCases", 1, 0, "NfcdDaemonHandler");
    qmlRegisterType<NfcdAdapterHandler>("ru.auroraos.NfcUseCases", 1, 0, "NfcdAdapterHandler");
    qmlRegisterType<NfcdTagHandler>("ru.auroraos.NfcUseCases", 1, 0, "NfcdTagHandler");
    qmlRegisterType<NfcdTagType2Handler>("ru.auroraos.NfcUseCases", 1, 0, "NfcdTagType2Handler");
    qmlRegisterType<NfcdIsoDepHandler>("ru.auroraos.NfcUseCases", 1, 0, "NfcdIsoDepHandler");
    qmlRegisterType<NfcdNdefHandler>("ru.auroraos.NfcUseCases", 1, 0, "NfcdNdefHandler");

    qmlRegisterUncreatableType<NfcdModeType>("ru.auroraos.NfcUseCases", 1, 0, "NfcdMode", "");
    qmlRegisterUncreatableType<NfcdTagProtocolType>("ru.auroraos.NfcUseCases", 1, 0,
                                                    "NfcdTagProtocol", "");
    qmlRegisterUncreatableType<NfcdTagTechnologyType>("ru.auroraos.NfcUseCases", 1, 0,
                                                      "NfcdTagTechnology", "");
    qmlRegisterUncreatableType<NfcdNdefFlagType>("ru.auroraos.NfcUseCases", 1, 0, "NfcdNdefFlag",
                                                 "");
    qmlRegisterUncreatableType<NfcdNdefTNFType>("ru.auroraos.NfcUseCases", 1, 0, "NfcdNdefTNF", "");

    qRegisterMetaType<NfcdMode>("NfcdMode");
    qRegisterMetaType<NfcdModes>("NfcdModes");
    qRegisterMetaType<NfcdTagProtocol>("NfcdTagProtocol");
    qRegisterMetaType<NfcdTagProtocols>("NfcdTagProtocols");
    qRegisterMetaType<NfcdTagTechnology>("NfcdTagTechnology");
    qRegisterMetaType<NfcdTagTechnologies>("NfcdTagTechnologies");
    qRegisterMetaType<NfcdNdefFlag>("NfcdNdefFlag");
    qRegisterMetaType<NfcdNdefFlags>("NfcdNdefFlags");
    qRegisterMetaType<NfcdNdefTNF>("NfcdNdefTNF");
    qRegisterMetaType<NfcdNdefTNFs>("NfcdNdefTNFs");
    qRegisterMetaType<NfcdIsoDepResult>("NdefIsoDepResult");

    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("NfcUseCases"));

    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/NfcUseCases.qml")));
    view->show();

    return application->exec();
}
