# NFC Use Cases

The project provides an example of working with NFC stack.

The main purpose is to show not only what features are available to work with these API,
but also how to use them correctly.

Build status:
1. example - [![pipeline status](https://gitlab.com/omprussia/demos/NfcUseCases/badges/example/pipeline.svg)](https://gitlab.com/omprussia/demos/NfcUseCases/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/demos/NfcUseCases/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/demos/NfcUseCases/-/commits/dev)

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

## Project Structure

The project has a standard structure of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.NfcUseCases.pro](ru.auroraos.NfcUseCases.pro)** file
  describes the project structure for the qmake build system.
* **[icons](icons)** directory contains the application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[components](app/qml/components)** directory contains the custom UI components.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[images](qml/images)** directory contains the additional custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[js](qml/js)** directory contains scripts for converting custom data types to strings.
  * **[NfcUseCases.qml](qml/ru.auroraos.NfcUseCases.qml)** file
    provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.NfcUseCases.spec](rpm/ru.auroraos.NfcUseCases.spec)** file is used by rpmbuild tool.
* **[src](src)** directory contains the C++ source code.
  * **[ru.auroraos.NfcUseCases.cpp](src/ru.auroraos.NfcUseCases.cpp)** file is the application entry point.
* **[translations](translations)** directory contains the UI translation files.
* **[ru.auroraos.NfcUseCases.desktop](ru.auroraos.NfcUseCases.desktop)** file
  defines the display and parameters for launching the application.
* **[submodules](submodules)** directory contains implementations of modules to work with NFC.
  * The [PCSC Lite](submodules/pcschandler) module provides the ability to detect the connection of
NFC tags and output a basic information on them. Also, when initializing this module, a list with
tag attributes and corresponding information is downloaded, which later also outputs on the screen.
  * The [NFCD](submodules/nfcdhandler) module is a DBus service for interaction with which a number
of [DBus interfaces](submodules/nfcdhandler/src/dbus) are implemented. A QML wrapper is implemented
for each interface class.

## Compatibility

The project is compatible with all current versions of the Aurora OS.

## Project Building

The project is built in the usual way using the Aurora SDK.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
