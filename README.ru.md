# NFC Use Cases

В проекте приведен пример работы со стеком NFC.

Основная цель - показать не только, какие функции доступны для
работы с этими API, но и как их правильно использовать.

Статус сборки:
1. example - [![pipeline status](https://gitlab.com/omprussia/demos/NfcUseCases/badges/example/pipeline.svg)](https://gitlab.com/omprussia/demos/NfcUseCases/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/demos/NfcUseCases/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/demos/NfcUseCases/-/commits/dev)

## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-Clause.md),
которая позволяет использовать его в сторонних приложениях.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

[Кодекс поведения](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

## Структура проекта

Проект имеет стандартную структуру приложения на базе C++ и QML для ОС Аврора.

* Файл **[ru.auroraos.NfcUseCases.pro](ru.auroraos.NfcUseCases.pro)**
        описывает структуру проекта для системы сборки qmake.
* Каталог **[icons](icons)** содержит иконки приложения для поддерживаемых разрешений экрана.
* Каталог **[qml](qml)** содержит исходный код на QML и ресурсы интерфейса пользователя.
    * Каталог **[components](app/qml/components)** содержит пользовательские компоненты пользовательского интерфейса.
    * Каталог **[cover](qml/cover)** содержит реализации обложек приложения.
    * Каталог **[images](qml/images)** содержит дополнительные иконки интерфейса пользователя.
    * Каталог **[pages](qml/pages)** содержит страницы приложения.
    * Каталог **[js](qml/js)** содержит скрипты для конвертации пользовательских типов данных в строки.
    * Файл **[NfcUseCases.qml](qml/NfcUseCases.qml)**
                предоставляет реализацию окна приложения.
* Каталог **[rpm](rpm)** содержит настройки сборки rpm-пакета.
    * Файл **[ru.auroraos.NfcUseCases.spec](rpm/ru.auroraos.NfcUseCases.spec)**
                используется инструментом rpmbuild.
* Каталог **[src](src)** содержит исходный код на C++.
    * Файл **[main.cpp](src/main.cpp)** является точкой входа в приложение.
* Каталог **[translations](translations)** содержит файлы перевода интерфейса пользователя.
* Файл **[ru.auroraos.NfcUseCases.desktop](ru.auroraos.NfcUseCases.desktop)**
        определяет отображение и параметры запуска приложения.
* Каталог **[submodules](submodules)** содержит реализацию модулей для работы с NFC.
    * Модуль [PCSC Lite](submodules/pcschandler) обеспечивает возможность обнаружения подключения
    NFC-меток и выводить основную информацию о них. Кроме того, при инициализации этого модуля загружается список
    атрибутов меток и соответствующей информации, который впоследствии также выводится на экран.
    * Модуль [NFCD](submodules/nfcdhandler) представляет собой DBus-сервис, для взаимодействия с которым реализован ряд
    интерфейсов [DBus](submodules/nfcdhandler/src/dbus). Для каждого класса интерфейса реализована QML-обертка.

## Совместимость

Проект совместим с актуальными версиями ОС Аврора.

## Сборка проекта

Проект собирается обычным образом с помощью Аврора SDK.

## Снимки экранов

![screenshots](screenshots/screenshots.png)

## This document in English

- [README.md](README.md)
